import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

THRESHOLD = 10
def read_data():
    timestamps = []
    values = []
    emas = []
    line = input()
    while line != 'THRESHOLD':
        line = input()
    global THRESHOLD
    THRESHOLD = float(input())
    print('Threshold', THRESHOLD)
    while True:
        line = input()
        while line != 'START':
            line = input()

        line = input()
        ema, fired = line.split(',')
        ema = float(ema)
        fired = fired == 'T'
        line = input()
        avgT = 0
        n = 0
        while line != 'END':
            t, val = line.split(',')
            t = int(t)
            val = float(val)
            timestamps.append(t)
            values.append(val)
            line = input()
            avgT += t
            n += 1
        avgT /= n
        emas.append((avgT, ema, fired))
        timestamps = timestamps[-100:]
        minT = min(timestamps)
        values = values[-100:]
        emas = [e for e in emas if e[0] > minT]
        yield (emas, (timestamps, values))

source = read_data()
def animate(i):
    emas, (xs, ys) = next(source)
    ax1.clear()
    ax1.set_ylim([-50, 150])
    ax1.set_autoscaley_on(False)
    ax1.plot(xs, ys, linewidth=0.5, marker='o', markersize=2)

    emasFired = [(t, ema) for (t, ema, fired) in emas if fired]
    emasNot = [(t, ema) for (t, ema, fired) in emas if not fired]

    ts, ema = zip(*[(t, ema) for (t, ema, _) in emas])
    ax1.plot(ts, ema, linewidth=0.5, c='k')
    if len(emasNot) > 0:
        t, e = zip(*emasNot)
        ax1.scatter(t, e, s=20, marker='x', c='k')
    if len(emasFired) > 0:
        t, e = zip(*emasFired)
        ax1.scatter(t, e, s=20, marker='x', c='g')

    ax1.scatter(ts, [e+THRESHOLD for e in ema], s=15, c='r', marker='_')

ani = animation.FuncAnimation(fig, animate, interval=1000)
plt.show()

