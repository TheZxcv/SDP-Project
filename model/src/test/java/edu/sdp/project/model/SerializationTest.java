package edu.sdp.project.model;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SerializationTest {

	private ObjectMapper mapper;

	@Before
	public void setUp() {
		mapper = new ObjectMapper();
		// deterministic properties order
		mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
	}

	@Test
	public void testPlayer() throws Exception {
		String json = "{\"address\":\"localhost\",\"name\":\"test\",\"port\":12345,\"ready\":false}";
		Player player = new Player("test", "localhost", 12345);

		assertEquals(player, mapper.readValue(json, Player.class));
		assertEquals(json, mapper.writeValueAsString(player));
	}

	@Test
	public void testGame() throws Exception {
		String json = "{\"name\":\"testgame\",\"players\":{\"0\":{\"address\":\"localhost\"," +
				"\"name\":\"test\",\"port\":12345,\"ready\":false}},\"size\":16,\"target\":100}";
		Game game = new Game("testgame", 16, 100);
		Player p1 = new Player("test", "localhost", 12345);
		game.addPlayer(p1);

		assertEquals(game, mapper.readValue(json, Game.class));
		assertEquals(json, mapper.writeValueAsString(game));
	}
}
