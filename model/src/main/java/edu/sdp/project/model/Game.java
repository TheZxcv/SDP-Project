package edu.sdp.project.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Game {

	private long nextId;
	private final String name;
	private final int size;
	private final int target;
	private final Map<Long, Player> players;

	@JsonCreator
	public Game(@JsonProperty("name") String name, @JsonProperty("size") int size, @JsonProperty("target") int target,
				@JsonProperty("players") Map<Long, Player> players) {
		if (size%2 != 0 || size < 2)
			throw new IllegalArgumentException("`size` must be even and greater than 1");
		if (target <= 0)
			throw new IllegalArgumentException("`target` must be even and greater than 0");
		this.name = name;
		this.size = size;
		this.target = target;
		this.players = players;
		this.nextId = availableNextId(players.keySet());
	}

	public Game(String name, int size, int target) {
		this(name, size, target, new HashMap<>());
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return size;
	}

	public int getTarget() {
		return target;
	}

	public boolean hasFreeSlots() {
		int freeSlots = (size * size) / 2 - players.size();
		return freeSlots > 0;
	}

	/**
	 * Adds a player and returns its ID.
	 * If <i>player</i> shares its name with a player already in the game
	 * or if the game is full, <i>player</i> won't be added to the game
	 * and -1 is returned.
	 *
	 * @param player - the player to be added
	 * @return the newly added player's ID or -1
	 */
	public long addPlayer(Player player) {
		if (!hasFreeSlots())
			return -1;

		if (!containsPlayer(player)) {
			long id = nextId++;
			players.put(id, player);
			return id;
		} else {
			return -1;
		}
	}

	public boolean removePlayer(String user) {
		long id = findPlayerId(user);
		if (id == -1)
			return false;
		else
			return players.remove(id) != null;
	}

	private boolean containsPlayer(Player player) {
		return findPlayerId(player.getName()) != -1;
	}

	public long findPlayerId(String name) {
		for (Entry<Long, Player> e : players.entrySet()) {
			Player p = e.getValue();
			if (p.getName().equals(name))
				return e.getKey();
		}
		return -1;
	}

	@JsonGetter("players")
	public Map<Long, Player> getPlayers() {
		return new HashMap<>(players);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;
		if (other instanceof Game) {
			Game that = (Game) other;
			if (!this.name.equals(that.name))
				return false;
			if (this.size != that.size)
				return false;
			return this.players.equals(that.players);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, size, players);
	}

	@Override
	public String toString() {
		return "Game [name=" + name + ", size=" + size + ", players=" + players + "]";
	}

	private long availableNextId(Collection<Long> ids) {
		return ids.stream().reduce(Long::max).orElse(-1L) + 1;
	}
}
