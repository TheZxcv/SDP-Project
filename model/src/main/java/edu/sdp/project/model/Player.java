package edu.sdp.project.model;

import java.net.InetSocketAddress;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Player {

	private final String name;
	private final InetSocketAddress address;
	private boolean ready;

	public Player(String name, InetSocketAddress address, boolean ready) {
		this.name = name;
		this.address = address;
		this.ready = ready;
	}

	public Player(String name, InetSocketAddress address) {
		this(name, address, false);
	}

	@JsonCreator
	public Player(@JsonProperty("name") String name, @JsonProperty("address") String address,
				  @JsonProperty("port") int port, @JsonProperty("ready") boolean ready) {
		this(name, new InetSocketAddress(address, port), ready);
	}

	public Player(String name, String address, int port) {
		this(name, new InetSocketAddress(address, port));
	}

	@JsonIgnore
	public InetSocketAddress getSocketAddress() {
		return address;
	}

	@JsonGetter("name")
	public String getName() {
		return name;
	}

	@JsonGetter("address")
	public String getAddress() {
		return address.getHostString();
	}

	@JsonGetter("port")
	public int getPort() {
		return address.getPort();
	}

	public void setReady() {
		this.ready = true;
	}

	@JsonGetter("ready")
	public boolean isReady() {
		return ready;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Player) {
			Player player = (Player) other;

			if (!this.name.equals(player.name))
				return false;
			if (this.ready != player.ready)
				return false;
			return this.address.equals(player.address);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, address);
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", address=" + address + "]";
	}
}
