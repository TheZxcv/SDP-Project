# Distributed multiplayer game [![build status](https://gitlab.com/TheZxcv/SDP-Project/badges/master/build.svg)](https://gitlab.com/TheZxcv/SDP-Project/commits/master)

Project assignment for course the _Distributed and Pervasive systems_ academic year 2016-2017.

The project consists on the development of a peer-to-peer online multiplayer game.

## Peer-to-peer network

The peer-to-peer network is organised as a **ring** overlay network.
The communication is **message-based** and mostly done in a **multicast** fashion,
except for the token exchange which happens strictly between two peers.
Each message is timestamped using a logical clock in a way that guarantees total order.

Multicast messages are delivered to the user ordered with respect to their timestamp,
the same applies for unicast messages, but this is not guaranteed between multicast and
unicast messages.

A **token** is used to achieve mutual exclusion.

## Server

A central server provides a web-based **RESTful** discovery service that allows new peers
to find and join a game. It keeps track of all the ongoing games and their players.


## Communication scheme

### Joining a game
 1. A peer a makes a `POST` request to the server providing its username, its
    local port and the game name:

    Target:
    ```
    SERVER-URI/games/{game}/{username}
    ```
    Data:
    ```json
    {
      "name" : "thisPlayer",
      "address" : "localhost",
      "port" : 12345,
      "ready" : false
    }
    ```

 2. The server adds--after checking the username uniqueness--the peer to the game's players
    list and replies with the game details:

    ```json
    {
      "name" : "GameName",
      "size" : 100,
      "players" : {
        "0" : {
          "name" : "aPlayer",
          "address" : "localhost",
          "port" : 54321,
          "ready" : true
        },
       "1" : {
          "name" : "thisPlayer",
          "address" : "localhost",
          "port" : 12345,
          "ready" : false
        }
      }
    }
    ```

 3. The peer connects and sends a bootstrap message consisting of its ID, address and port
    to a peer chosen randomly among the ones who are ready (**entrypeer**):

    ```json
    {
      "type" : "bootstrap",
      "senderId" : 0,
      "timestamp" : -1,
      "address" : "localhost",
      "port" : 12345
    }
    ```

 4. The entrypeer enqueues the bootstrap message. Once it receives the token, it broadcasts
    the request in a special message to all the other peers and replies to the peer with
    the list of IDs of the current players in the game;
 5. All peers (including the entrypeer) proceed to connect to the newly arrived peer;
 6. The peer awaits for all the connection from the other peers;
 7. After that, the peer makes a `PUT` request to the server to let it know that the joining
    procedure was successful and it can receive joining requests from other peers;
 7. The joining procedure is concluded.

##### Note:
Other peers can join or leave the game while the new peer's join request is enqueued but
not after the entrypeer has broadcasted it up until the new peer hasn't effectively joined
the game.

### Leaving a game
 1. The peer that wants to leave the game awaits for the token;
 2. Once it has the token, it broadcasts a peer-leaving message and awaits to receive it;
 3. When it receives its own peer-leaving message, it leaves the P2P network and informs the
    server, all without passing on the token!
 4. When the other peers receive the peer-leaving message, they check if the peer
    that is leaving is previous to them in the ring overlay, in which case they create a
    new token and send it to themselves.

##### Note:
Other peers can't join or leave the game while the leaving peer owns the token.
