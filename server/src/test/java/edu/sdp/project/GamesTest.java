package edu.sdp.project;

import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;
import org.junit.Before;
import org.junit.Test;

public class GamesTest {

	private GameResource res;
	private Games games;

	@Before
	public void reset() {
		res = new GameResource();
		games = Games.getInstance();
		games.reset();
	}

	@Test
	public void testNoGames() throws Exception {
		Response response = res.getAllGames();

		assertEquals(Status.OK.getStatusCode(), response.getStatus());
		assertEquals(Collections.emptyList(), response.getEntity());
	}

	@Test
	public void testInsertGame() throws Exception {
		String name = "test";
		Response response = res.createGame(name, new Game(name, 10, 10));

		assertEquals(Status.CREATED.getStatusCode(), response.getStatus());

		response = res.getAllGames();

		assertEquals(singletonList(name), response.getEntity());
	}

	@Test
	public void testJoinGame() throws Exception {
		String name = "test";
		int size = 10;
		int points = 100;
		Game game = new Game(name, size, points);
		Response response = res.createGame(name, new Game(name, size, points));

		assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
		assertEquals(game, games.getGame(name));


		Player p = new Player("blank", "localhost", 12345);
		game.addPlayer(p);
		HttpServletRequest req = mock(HttpServletRequest.class);
		when(req.getRemoteAddr()).thenReturn(p.getAddress());

		response = res.joinGame(req, name, p);

		assertEquals(Status.OK.getStatusCode(), response.getStatus());
		assertEquals(game, response.getEntity());
	}

	@Test
	public void testJoinFullGame() throws Exception {
		String name = "test";
		Game game = mock(Game.class);
		when(game.getName()).thenReturn(name);
		when(game.addPlayer(any())).thenReturn(-1L);
		when(game.hasFreeSlots()).thenReturn(false);

		games.putGame(game);

		Player p = new Player("blank", "localhost", 12345);
		HttpServletRequest req = mock(HttpServletRequest.class);
		when(req.getRemoteAddr()).thenReturn(p.getAddress());

		Response response = res.joinGame(req, name, p);

		assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}
}
