package edu.sdp.project;

import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/games")
public class GameResource {

	private static final Games games = Games.getInstance();
	private static final Logger logger = Logger.getLogger(GameResource.class.getName());

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllGames() {
		logger.info("Request: get all games");
		return Response.ok().entity(games.getGameIds()).build();
	}

	@GET
	@Path("{game}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getGame(@PathParam("game") String gamename) {
		final Game game = games.getGame(gamename);
		if (game != null) {
			synchronized (game) {
				logger.info("Request: get game's -> OK!");
				return Response.ok().entity(game).build();
			}
		} else {
			logger.info("Request: get game's -> Not Found!");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@POST
	@Path("{game}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createGame(@PathParam("game") String name, Game game) {
		String message = "Request: create game -> ";
		if (!name.equals(game.getName())) {
			logger.info(message + "BAD, conflicting names!");
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			boolean ret = games.putGame(game);
			if (ret) {
				logger.info(message + "OK!");
				return Response.status(Status.CREATED).build();
			} else {
				logger.info(message + "DUPLICATED!");
				return Response.status(Status.CONFLICT).build();
			}
		} catch (IllegalArgumentException e) {
			logger.info(message + "BAD!");
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("{game}/{user}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response joinGame(@Context HttpServletRequest request, @PathParam("game") String gamename, Player player) {
		String message = "Request: join a game -> ";
		final Game game = games.getGame(gamename);
		if (game != null) {
			synchronized (game) {
				if (game.addPlayer(player) >= 0) {
					logger.info(message + "OK!");
					return Response.ok().entity(game).build();
				} else {
					if (game.hasFreeSlots()) {
						logger.info(message + "DUPLICATED!");
						return Response.status(Status.CONFLICT).build();
					} else {
						logger.info(message + "GAME FULL!");
						return Response.status(Status.BAD_REQUEST).build();
					}
				}
			}
		} else {
			logger.info(message + "NOT FOUND!");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@PUT
	@Path("{game}/{user}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response playerReady(@Context HttpServletRequest request, @PathParam("game") String gamename, Player player) {
		String message = "Request: player ready -> ";
		final Game game = games.getGame(gamename);
		if (game != null) {
			synchronized (game) {
				long id = game.findPlayerId(player.getName());
				if (id < -1) {
					logger.info(message + "BAD!");
					return Response.status(Status.BAD_REQUEST).build();
				} else {
					Player p = game.getPlayers().get(id);
					p.setReady();
					logger.info(message + "OK!");
					return Response.ok().entity(game).build();
				}
			}
		} else {
			logger.info(message + "NOT FOUND!");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@DELETE
	@Path("{game}/{user}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response leaveGame(@PathParam("game") String gamename, @PathParam("user") String user) {
		String message = "Request: leave game -> ";
		final Game game = games.getGame(gamename);
		if (game != null) {
			synchronized (game) {
				if (game.removePlayer(user)) {
					logger.info(message + "OK!");
					if (game.getPlayers().size() == 0) {
						logger.info(message + "removing empty game.");
						games.removeGame(game.getName());
					}
					return Response.ok().build();
				} else {
					logger.info(message + "BAD!");
					return Response.status(Status.BAD_REQUEST).build();
				}
			}
		} else {
			logger.info(message + "NOT FOUND!");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	@DELETE
	@Path("{game}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response removeGame(@PathParam("game") String name) {
		if (games.removeGame(name)) {
			logger.info("Request: delete game -> OK!");
			return Response.ok().build();
		} else {
			logger.info("Request: delete game -> NOT FOUND!");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

}
