package edu.sdp.project;

import edu.sdp.project.model.Game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Games {

	private static final class Holder {
		private static final Games INSTANCE = new Games();
	}

	private final Map<String, Game> games;

	private Games() {
		this.games = new HashMap<>();
	}

	public synchronized boolean putGame(Game game) {
		if (!games.containsKey(game.getName())) {
			games.put(game.getName(), game);
			return true;
		} else {
			return false;
		}
	}

	public synchronized Game getGame(String name) {
		return games.get(name);
	}

	public synchronized boolean removeGame(String name) {
		return games.remove(name) != null;
	}

	public synchronized List<String> getGameIds() {
		return new ArrayList<>(games.keySet());
	}

	public synchronized void reset() {
		games.clear();
	}

	public static Games getInstance() {
		return Holder.INSTANCE;
	}

}
