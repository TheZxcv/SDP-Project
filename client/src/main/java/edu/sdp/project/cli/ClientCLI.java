package edu.sdp.project.cli;

import edu.sdp.project.client.GamesClient;
import edu.sdp.project.game.GameLoop;
import edu.sdp.project.game.GameUtils;
import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;
import edu.sdp.project.utils.Utils;

import javax.ws.rs.ProcessingException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;

public class ClientCLI {

	private String username;
	private GamesClient client;
	private ServerSocket ss;
	private final Map<String, Consumer<String[]>> commands;
	private final Console console = Console.getDefault();

	public ClientCLI() {
		commands = new TreeMap<>();
		commands.put("list", this::list);
		commands.put("create", this::create);
		commands.put("delete", this::delete);
		commands.put("join", this::join);
		commands.put("leave", this::leave);
		commands.put("info", this::info);
		commands.put("help", this::help);
		commands.put("exit", this::exit);
	}

	private void help(String[] args) {
		checkArgs(0, args);

		console.println("Available commands: ");
		for (String name : commands.keySet())
			console.println("\t- " + name);
	}

	private void exit(String[] args) {
		checkArgs(0, args);

		try {
			console.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	private void leave(String[] args) throws IllegalArgumentException {
		checkArgs(1, args);
		client.leaveGame(args[0], username);
	}

	private void info(String[] args) throws IllegalArgumentException {
		checkArgs(1, args);

		console.println((client.getGame(args[0])).toString());
	}

	private void join(String[] args) throws IllegalArgumentException {
		checkArgs(1, args);

		try {
			if (ss == null || ss.isClosed())
				ss = new ServerSocket(0);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Player me = new Player(username, Utils.getLocalAddress().getHostAddress(), ss.getLocalPort());
			Game game = client.joinGame(args[0], me);
			GameLoop gl = GameUtils.joinGame(client, game, username, ss);
			gl.setConsole(console);
			gl.run();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	private void delete(String[] args) throws IllegalArgumentException {
		checkArgs(1, args);

		client.removeGame(args[0]);
	}

	private void list(String[] args) throws IllegalArgumentException {
		checkArgs(0, args);

		for (String name : client.getAllGames())
			console.println(name);
	}

	private void create(String[] args) throws IllegalArgumentException {
		checkArgs(3, args);

		client.createGame(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		console.println("game successfully created!");
	}

	public void start() throws Exception {
		String server = ask(console, "Server: ");
		int port = askPort(console, "Port: ");
		client = new GamesClient(server, port);
		do
			username = ask(console, "Username: ");
		while (username.isEmpty());

		help(new String[]{});
		String line;
		while ((line = console.read()) != null) {
			String[] args = line.split(" ");
			String cmdName = args[0];
			args = Arrays.copyOfRange(args, 1, args.length);
			Consumer<String[]> cmd = commands.get(cmdName);
			if (cmd != null) {
				try {
					cmd.accept(args);
				} catch (IllegalArgumentException e) {
					console.println(e.getMessage());
				} catch (ProcessingException e) {
					// unwrap the exception and rethrow it
					// (only if it's an actual Exception and not a Throwable)
					if (e.getCause() instanceof Exception)
						throw (Exception) e.getCause();
					else
						throw e;
				}
			} else {
				console.println("Unrecognized command.");
			}
		}
	}

	private static void checkArgs(int expected, String... args) throws IllegalArgumentException {
		if (expected != args.length)
			throw new IllegalArgumentException("wrong number of parameters");
	}


	private static int askPort(Console console, String question) throws IOException {
		do {
			try {
				int port = Integer.parseInt(ask(console, question));
				if (port < 1 || port > 65535)
					throw new NumberFormatException();
				return port;
			} catch (NumberFormatException e) {
				console.println("Invalid port number!");
			}
		} while (true);
	}

	private static String ask(Console console, String question) throws IOException {
		console.println(question);
		String input = console.read();
		if (input == null)
			throw new IOException("Stream closed");
		return input;
	}

}
