package edu.sdp.project.cli;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class Console implements Closeable {

	private static final class Holder {
		private static Console INSTANCE = new Console(System.in, System.out);
	}

	private static final String PROMPT = "> ";

	private final InterruptibleConsoleReader reader;
	private final PrintStream writer;
	private boolean promptOnScreen;

	public Console(InputStream in, PrintStream out) {
		this.reader = new InterruptibleConsoleReader(in);
		this.writer = out;
		this.promptOnScreen = false;
	}

	public Console(InputStream in, OutputStream out) {
		this(in, new PrintStream(out));
	}

	public String read() {
		enablePrompt();
		String line = reader.read();
		disablePrompt();
		return line;
	}

	private synchronized void enablePrompt() {
		writer.print(PROMPT);
		promptOnScreen = true;
	}

	private synchronized void disablePrompt() {
		promptOnScreen = false;
	}

	public void interruptConsumer() {
		reader.interruptConsumer();
	}

	public synchronized void println(String message) {
		if (promptOnScreen) {
			writer.println("\r" + message);
			writer.flush();
			enablePrompt();
		} else {
			writer.println(message);
		}
	}

	@Override
	public void close() throws IOException {
		reader.close();
		writer.close();
	}

	public static Console getDefault() {
		return Holder.INSTANCE;
	}
}
