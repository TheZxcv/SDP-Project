package edu.sdp.project.cli;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;
import java.util.logging.Logger;

public class InterruptibleConsoleReader implements Closeable {

	private static final Logger logger = Logger.getLogger(InterruptedException.class.getName());

	private final BufferedReader reader;
	private final Deque<Optional<String>> buffer;
	private final Thread self;

	private int neededLines = 0;
	private final Object needInput = new Object();

	private volatile boolean alive;
	private volatile boolean closed;

	public InterruptibleConsoleReader(BufferedReader reader) {
		this.reader = reader;
		this.buffer = new ArrayDeque<>();
		this.self = new Thread(this::loop);
		self.setName(getClass().getName());
	}

	public InterruptibleConsoleReader(Reader reader) {
		this(new BufferedReader(reader));
	}

	public InterruptibleConsoleReader(InputStream in) {
		this(new InputStreamReader(in));
	}

	private void loop() {
		try {
			alive = true;
			closed = false;
			String line;
			while (alive) {
				synchronized (needInput) {
					// a spurious wake-ups can happen
					while (neededLines == 0)
						needInput.wait();
				}
				if (!alive)
					continue;

				line = reader.readLine();
				if (line == null) {
					closed = true;
					alive = false;
				} else {
					synchronized (needInput) {
						put(Optional.of(line));
						neededLines--;
						if (neededLines < 0)
							neededLines = 0;
					}
				}
			}
			put(Optional.empty());
		} catch (IOException e) {
			logger.info(e.getMessage());
		} catch (InterruptedException e) {
			logger.warning(e.getMessage());
		}
		logger.info("Closed!");
	}

	private void put(Optional<String> line) {
		synchronized (buffer) {
			buffer.offer(line);
			buffer.notify();
		}
	}

	private void putFirst(Optional<String> line) {
		synchronized (buffer) {
			buffer.addFirst(line);
			buffer.notify();
		}
	}

	private String take() throws InterruptedException {
		synchronized (buffer) {
			Optional<String> s = buffer.poll();
			if (s == null) {
				synchronized (needInput) {
					neededLines++;
					needInput.notify();
				}
				// a spurious wake-ups can happen
				do {
					buffer.wait();
					s = buffer.poll();
				} while(s == null);
			}
			return s.orElse(null);
		}
	}

	public void interruptConsumer() {
		synchronized (needInput) {
			if (neededLines > 0) {
				putFirst(Optional.empty());
				neededLines--;
			}
		}
	}

	public String read() {
		if (!alive && !closed) {
			self.start();
		} else if (closed && buffer.isEmpty()) {
			return null;
		}

		try {
			return take();
		} catch (InterruptedException e) {
			logger.warning(e.getMessage());
		}
		return null;
	}

	@Override
	public void close() throws IOException {
		alive = false;
		closed = true;
		reader.close();
		synchronized (needInput) {
			neededLines = -1;
			needInput.notify();
		}
		try {
			self.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
