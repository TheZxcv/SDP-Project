package edu.sdp.project.sensors;

import edu.sdp.project.game.Area;
import edu.sdp.project.game.Bomb;
import edu.sdp.project.utils.BlockingQueueWrapper;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import static edu.sdp.project.utils.Utils.sleepWithoutHassle;
import static java.lang.Math.ceil;

public class BombFinder {

	private static interface DataPlotter {
		void print(double ema, List<Measurement> data, boolean fired);
	}

	private static class NullDataPlotter implements DataPlotter {
		@Override
		public void print(double ema, List<Measurement> data, boolean fired) {}
	}

	private static class StreamDataPlotter implements DataPlotter {
		private final PrintStream out;

		private StreamDataPlotter(PrintStream out) {
			this.out = out;
		}

		@Override
		public void print(double ema, List<Measurement> data, boolean fired) {
			out.println("START");
			out.println(ema + "," + (fired ? "T" : "F"));
			for (Measurement m : data)
				out.println(m.getTimestamp() + "," + m.getValue());
			out.println("END");
		}
	}

	private static final Logger logger = Logger.getLogger(BombFinder.class.getName());

	private static final double ALPHA = 0.30;
	private static final double THRESHOLD = 5;
	private static DataPlotter plotter = new NullDataPlotter();

	private final Simulator sim;
	private final Buffer<Measurement> queue;
	private final BlockingQueue<Bomb> output;
	private final Thread self;

	private int nextBombId = 0;

	private volatile boolean alive;

	public BombFinder(BlockingQueue<Bomb> output) {
		this.output = output;
		this.queue = new QueueBackedBuffer<>();
		this.sim = new AccelerometerSimulator(queue);
		this.alive = false;
		this.self = new Thread(this::loop);
		self.setName("BombFinder");
	}

	private void loop() {
		new Thread(null, sim, "Simulator").start();

		List<Measurement> data = nextData();
		// you'd better not be empty!
		assert !data.isEmpty();
		double ema = avg(data);
		plotter.print(ema, data, false);
		while (alive) {
			data = nextData();
			if (data.isEmpty())
				// let's wait some more
				continue;
			double m = avg(data);
			double nextEma = ema + ALPHA * (m - ema);
			boolean anomaly = nextEma - ema > THRESHOLD;
			if (anomaly) {
				emitBomb((int) ceil(nextEma));
			}
			plotter.print(nextEma, data, anomaly);
			ema = nextEma;
		}
	}

	private void emitBomb(int kind) {
		try {
			output.put(new Bomb(nextBombId++, Area.of(kind)));
		} catch (InterruptedException e) {
			logger.warning("Failed to emit bomb");
		}
	}

	private List<Measurement> nextData() {
		sleepWithoutHassle(1000);
		return queue.readAllAndClean();
	}

	public void start() {
		alive = true;
		self.start();
	}

	public void shutdown() throws InterruptedException {
		alive = false;
		sim.stopMeGently();
		if (self.isAlive())
			self.join();
	}

	private static double avg(List<Measurement> values) {
		return values.stream()
				.mapToDouble(Measurement::getValue)
				.average()
				.getAsDouble();
	}

	public static void main(String[] args) throws Exception {
		System.out.println("THRESHOLD");
		System.out.println(THRESHOLD);
		plotter = new StreamDataPlotter(System.out);
		BombFinder b = new BombFinder(new BlockingQueueWrapper<>(new LinkedList<>()));
		b.start();
		b.self.join();
	}
}
