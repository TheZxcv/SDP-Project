package edu.sdp.project.sensors;

import edu.sdp.project.utils.BlockingQueueWrapper;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class QueueBackedBuffer<T extends Measurement> implements Buffer<T> {

	private final BlockingQueue<T> queue;

	public QueueBackedBuffer() {
		this.queue = new BlockingQueueWrapper<>(new ArrayDeque<>());
	}

	@Override
	public void addNewMeasurement(T t) {
		try {
			queue.put(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<T> readAllAndClean() {
		List<T> list = new ArrayList<>();
		queue.drainTo(list);
		return list;
	}
}
