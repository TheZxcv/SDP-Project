package edu.sdp.project.channels;

import edu.sdp.project.multicast.messages.Message;

public interface Channel {
	public void sendMessage(Message message);

	public Message receiveMessage();

	public void close();
}
