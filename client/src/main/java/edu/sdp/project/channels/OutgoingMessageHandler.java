package edu.sdp.project.channels;

import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import edu.sdp.project.multicast.messages.Message;
import edu.sdp.project.multicast.messages.MessageWriter;
import edu.sdp.project.multicast.messages.Poison;

public class OutgoingMessageHandler implements Runnable {

	private static final Logger logger = Logger.getLogger(OutgoingMessageHandler.class.getName());

	private final OutputStream out;
	private final BlockingQueue<Message> queue;

	public OutgoingMessageHandler(OutputStream out, BlockingQueue<Message> queue) {
		this.out = out;
		this.queue = queue;
	}

	@Override
	public void run() {
		MessageWriter writer = new MessageWriter(out);
		try {
			boolean alive = true;
			while (alive) {
				Message msg = queue.take();
				if (msg instanceof Poison) {
					writer.flush();
					alive = false;
					logger.info("I have been poisoned");
				} else
					writer.write(msg);
			}
		} catch (IOException | InterruptedException e) {
			if (e instanceof SocketException) {
				try {
					writer.flush();
					writer.close();
				} catch (IOException err) {
					logger.warning(err.toString());
				}
				logger.info("Gracefully closing...");
			} else {
				logger.warning(e.toString());
			}
		}
	}
}