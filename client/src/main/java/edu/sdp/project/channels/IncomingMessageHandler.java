package edu.sdp.project.channels;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import edu.sdp.project.multicast.messages.Message;
import edu.sdp.project.multicast.messages.MessageReader;

public class IncomingMessageHandler implements Runnable {

	private static final Logger logger = Logger.getLogger(IncomingMessageHandler.class.getName());

	private final InputStream in;
	private final BlockingQueue<Message> queue;

	public IncomingMessageHandler(InputStream in, BlockingQueue<Message> queue) {
		this.in = in;
		this.queue = queue;
	}

	@Override
	public void run() {
		MessageReader reader = new MessageReader(in);
		try {
			boolean alive = true;
			while (alive) {
				Message msg = reader.read();
				if (msg != null) {
					queue.put(msg);
				} else {
					alive = false;
					logger.info("Input channel is closed");
				}
			}
		} catch (SocketException e) {
			logger.info("Gracefully closing...");
		} catch (IOException | InterruptedException e) {
			if (e instanceof IOException && e.getMessage().contains("end-of-input")) {
				logger.info("Gracefully closing...");
			} else {
				e.printStackTrace();
			}
		}
	}
}
