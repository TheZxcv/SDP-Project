package edu.sdp.project.channels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import edu.sdp.project.multicast.messages.Message;

public class BroadcastChannel implements Channel {

	private final List<Channel> channels;

	public BroadcastChannel() {
		this(Collections.emptyList());
	}

	public BroadcastChannel(Collection<Channel> channels) {
		this.channels = new ArrayList<>(channels);
	}

	public void addChannel(Channel c) {
		channels.add(c);
	}

	public boolean removeChannel(Channel c) {
		return channels.remove(c);
	}

	@Override
	public void sendMessage(Message message) {
		List<Thread> threads = new ArrayList<>(channels.size());
		for (Channel c : channels) {
			Thread t = new Thread(() -> c.sendMessage(message));
			t.setName("Broadcaster");
			t.start();
			threads.add(t);
		}

		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				throw new AssertionError(e);
			}
		}
	}

	@Override
	public Message receiveMessage() {
		throw new IllegalStateException("Not implemented yet");
	}

	/**
	 * Does not close any of the backing channels.
	 */
	@Override
	public void close() {
		channels.clear();
	}
}
