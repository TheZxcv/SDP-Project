package edu.sdp.project.channels;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import edu.sdp.project.multicast.messages.Message;
import edu.sdp.project.multicast.messages.Poison;
import edu.sdp.project.utils.SynchronizedQueue;

public class SocketChannel implements Channel, Closeable {

	private static final Logger logger = Logger.getLogger(SocketChannel.class.getName());

	private final long peerId;
	private final Socket sock;

	private final BlockingQueue<Message> incomingQueue;
	private final BlockingQueue<Message> outgoingQueue;

	private final Thread incoming;
	private final Thread outgoing;

	public SocketChannel(long id, long peerId, Socket sock) {
		this(id, peerId, sock, new SynchronizedQueue<>());
	}

	public SocketChannel(long id, long peerId, Socket sock, BlockingQueue<Message> outputQueue) {
		this.peerId = peerId;
		this.sock = sock;

		this.incomingQueue = outputQueue;
		this.outgoingQueue = new SynchronizedQueue<>();

		IncomingMessageHandler in = null;
		OutgoingMessageHandler out = null;
		try {
			in = new IncomingMessageHandler(sock.getInputStream(), incomingQueue);
			out = new OutgoingMessageHandler(sock.getOutputStream(), outgoingQueue);
		} catch (IOException e) {
			logger.severe(e.toString());
		} finally {
			incoming = new Thread(in);
			incoming.setName("Thread-" + id + "-" + peerId + "-Incoming");
			outgoing = new Thread(out);
			outgoing.setName("Thread-" + id + "-" + peerId + "-Outgoing");
		}

		incoming.start();
		outgoing.start();
	}

	public void close() {
		try {
			/*
			 * send a poisoned message and then wait for all the messages to be
			 * sent
			 */
			sendMessage(new Poison());
			outgoing.join();

			sock.close();
			incoming.join();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendMessage(Message message) {
		try {
			outgoingQueue.put(message);
		} catch (InterruptedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public Message receiveMessage() {
		try {
			return incomingQueue.take();
		} catch (InterruptedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public String toString() {
		return "PeerChannel [peerId=" + peerId + ", sock=" + sock.getInetAddress() + ":" + sock.getPort() + "]";
	}
}