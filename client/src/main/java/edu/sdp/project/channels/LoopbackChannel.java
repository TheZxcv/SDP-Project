package edu.sdp.project.channels;

import edu.sdp.project.multicast.messages.Message;

import java.util.concurrent.BlockingQueue;

public class LoopbackChannel implements Channel {
	private final BlockingQueue<Message> queue;

	public LoopbackChannel(BlockingQueue<Message> outputQueue) {
		this.queue = outputQueue;
	}

	@Override
	public void sendMessage(Message message) {
		try {
			queue.put(message);
		} catch (InterruptedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public Message receiveMessage() {
		try {
			return queue.take();
		} catch (InterruptedException e) {
			throw new AssertionError(e);
		}
	}

	@Override
	public void close() {
		;
	}
}
