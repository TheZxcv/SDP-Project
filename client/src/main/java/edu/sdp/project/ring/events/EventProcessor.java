package edu.sdp.project.ring.events;

public interface EventProcessor {
	public void process(TokenExchange event);

	public void process(NodeJoining event);

	public void process(NodeLeaving event);

	public void process(UserEvent event);

	public void process(Event event);
}
