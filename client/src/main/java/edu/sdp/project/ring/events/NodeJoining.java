package edu.sdp.project.ring.events;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.sdp.project.multicast.messages.BootstrapMessage;

public class NodeJoining extends Event {

	public final BootstrapMessage bootstrap;

	public NodeJoining(@JsonProperty("bootstrap") BootstrapMessage bootstrap) {
		this.bootstrap = bootstrap;
	}

	@Override
	public void process(EventProcessor processor) {
		processor.process(this);
	}

	@Override
	public String toString() {
		return "NodeJoining [" + bootstrap + "]";
	}
}
