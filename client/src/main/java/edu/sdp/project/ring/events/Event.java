package edu.sdp.project.ring.events;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value=TokenExchange.class, name="token"),
	@JsonSubTypes.Type(value=NodeJoining.class, name="joining"),
	@JsonSubTypes.Type(value=NodeLeaving.class, name="leaving"),
	@JsonSubTypes.Type(value=UserEvent.class, name="user")
})

public abstract class Event {
	public void process(EventProcessor processor) {
		processor.process(this);
	}
}
