package edu.sdp.project.ring.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.sdp.project.game.events.GameEvent;

public class UserEvent extends Event {

	public final GameEvent event;

	@JsonCreator
	public UserEvent(@JsonProperty("event") GameEvent event) {
		this.event = event;
	}


	@Override
	public void process(EventProcessor processor) {
		processor.process(this);
	}

	@Override
	public String toString() {
		return "UserEvent [user-event=" + event + "]";
	}
}
