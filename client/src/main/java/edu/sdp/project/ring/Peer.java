package edu.sdp.project.ring;

import edu.sdp.project.game.events.GameEvent;
import edu.sdp.project.ring.events.NodeLeaving;
import edu.sdp.project.ring.events.TokenExchange;
import edu.sdp.project.ring.events.UserEvent;

public abstract class Peer {

	private final Ring ring;
	private Token token;

	public Peer(Ring ring) {
		this.ring = ring;
		ring.attachPeer(this);
	}

	protected void setToken(Token token) {
		this.token = token;
	}

	public abstract void onStart();

	public abstract void onEvent(GameEvent event);

	public abstract void onToken();

	public void send(GameEvent ge) {
		ring.sendToEveryone(new UserEvent(ge));
	}

	public void leave() {
		ring.sendToEveryone(new NodeLeaving(ring.getId()));
	}

	public void passToken() {
		if (token.isSpinning())
			ring.sendToNext(new TokenExchange());
	}

	public void lockToken() throws InterruptedException, IllegalStateException {
		token.tryLock();
	}

	public void unlockToken() {
		token.unlock();
	}

	public boolean isTokenSpinning() {
		return token.isSpinning();
	}

	/**
	 * Let the token spin once.
	 * Used only when there is only one peer and the token is not spinning.
	 */
	public void spinTokenOnce() {
		assert !token.isSpinning();
		ring.sendToNext(new TokenExchange());
	}
}
