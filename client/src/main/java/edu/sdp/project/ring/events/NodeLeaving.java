package edu.sdp.project.ring.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NodeLeaving extends Event {

	public final long id;

	public NodeLeaving(@JsonProperty("id") long id) {
		this.id = id;
	}

	@Override
	public void process(EventProcessor processor) {
		processor.process(this);
	}

	@Override
	public String toString() {
		return "NodeLeaving [id=" + id + "]";
	}
}
