package edu.sdp.project.ring;

import edu.sdp.project.ring.events.Event;

import java.io.IOException;

public interface Ring {
	public void attachPeer(Peer peer);

	public long getId();

	public int size();

	public void start();

	public void waitUntilReady() throws InterruptedException;

	public void waitUntilClosed() throws InterruptedException;

	public void sendToNext(Event event);

	public void sendToPrev(Event event);

	public void sendToEveryone(Event event);

	public void close() throws IOException;
}
