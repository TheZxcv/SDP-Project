package edu.sdp.project.ring;

/**
 * This class is mainly used to avoid useless token spinning when
 * the ring consists of only one peer.
 */
public class Token {

	private boolean tokenHeld;
	private boolean tokenInUse;

	private final Object lock = new Object();

	public Token() {
		this.tokenHeld = false;
		this.tokenInUse = false;
	}

	/**
	 * Check whether the token is spinning around the ring or not.
	 * The token does not continuously spin when there is only one peer
	 * in the ring.
	 *
	 * @return whether the token is spinnig or not.
	 */
	public boolean isSpinning() {
		synchronized (lock) {
			return !tokenHeld;
		}
	}

	/**
	 * Tries to lock the token.
	 * It can fail if the token is or restarts spinning.
	 *
	 * @throws IllegalStateException - if the token is spinning
	 * @throws InterruptedException
	 */
	public void tryLock() throws InterruptedException {
		synchronized (lock) {
			// a spurious wake-ups can happen
			while (tokenInUse)
				lock.wait();
			if (!tokenHeld)
				throw new IllegalStateException("This token cannot be locked because it's spinning");
			tokenInUse = true;
		}
	}

	/**
	 * Tries to unlock the token.
	 * It fails if the token is spinning.
	 *
	 * @throws IllegalStateException - if the token is spinning
	 */
	public void unlock() {
		synchronized (lock) {
			if (!tokenHeld)
				throw new IllegalStateException("This token cannot be unlocked because it's spinning");
			tokenInUse = false;
			lock.notify();
		}
	}

	/**
	 * Sets the token as not spinning.
	 * This method is called when there is only one peer left in the ring.
	 *
	 * @throws InterruptedException
	 */
	public void stopSpinning() throws InterruptedException {
		synchronized (lock) {
			tokenHeld = true;
			lock.notify();
		}
	}

	/**
	 * Sets the token as spinning.
	 * This method is called when a peer joins a ring that consisted of just one peer.
	 *
	 * @throws InterruptedException
	 */
	public void startSpinning() throws InterruptedException {
		synchronized (lock) {
			// a spurious wake-ups can happen
			while (tokenInUse)
				lock.wait();
			tokenHeld = false;
			lock.notify();
		}
	}
}
