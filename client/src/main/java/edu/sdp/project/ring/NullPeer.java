package edu.sdp.project.ring;

import edu.sdp.project.game.events.GameEvent;

public class NullPeer extends Peer {

	public NullPeer(Ring ring) {
		super(ring);
	}

	@Override
	public void onStart() {}

	@Override
	public void onToken() {}

	@Override
	public void onEvent(GameEvent event) {}

}
