package edu.sdp.project.ring.events;

public class TokenExchange extends Event {

	public TokenExchange() {}

	@Override
	public void process(EventProcessor processor) {
		processor.process(this);
	}

	@Override
	public String toString() {
		return "TokenExchange []";
	}
}
