package edu.sdp.project.ring;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Logger;

import edu.sdp.project.channels.Channel;
import edu.sdp.project.channels.SocketChannel;
import edu.sdp.project.game.events.PlayerLeft;
import edu.sdp.project.multicast.Multicast;
import edu.sdp.project.multicast.messages.BootstrapMessage;
import edu.sdp.project.multicast.messages.Message;
import edu.sdp.project.multicast.messages.PeerListMessage;
import edu.sdp.project.multicast.messages.UnicastMessage;
import edu.sdp.project.ring.events.Event;
import edu.sdp.project.ring.events.EventProcessor;
import edu.sdp.project.ring.events.NodeJoining;
import edu.sdp.project.ring.events.NodeLeaving;
import edu.sdp.project.ring.events.TokenExchange;
import edu.sdp.project.ring.events.UserEvent;
import edu.sdp.project.utils.BlockingQueueWrapper;
import edu.sdp.project.utils.Pair;

import static edu.sdp.project.utils.Utils.getLocalAddress;

public class RingImpl implements Ring, EventProcessor {

	private static final Logger logger = Logger.getLogger(RingImpl.class.getName());

	private final long id;
	private final ServerSocket ss;
	private final Multicast multicast;
	private final List<Long> channels;
	private final Queue<Pair<Channel, BootstrapMessage>> requests;
	private final Thread acceptor;
	private final Thread self;

	private final Token token;
	private Peer peer;
	private volatile boolean alive;
	private volatile boolean closed;

	public RingImpl(long id, ServerSocket ss, Map<Long, InetSocketAddress> others) throws EntryPeerUnreachableException {
		this.id = id;
		this.ss = ss;

		Map<Long, Socket> sockets = null;
		try {
			sockets = bootstrap(id, others);
		} catch (IOException e) {
			logger.warning(e.toString());
			throw new EntryPeerUnreachableException();
		}
		this.multicast = new Multicast(id, sockets);
		this.requests = new BlockingQueueWrapper<>(new LinkedList<>());
		this.peer = new NullPeer(this);
		this.token = new Token();
		multicast.init();

		this.acceptor = new Thread(this::loopAccept);
		this.acceptor.setName("acceptor-" + id);
		this.self = new Thread(this::loop);
		this.self.setName("ring-" + id);
		this.channels = multicast.getChannelList();
		this.alive = false;
		this.closed = false;
	}

	public RingImpl(long id, Map<Long, InetSocketAddress> others) throws EntryPeerUnreachableException {
		this(id, createServerSocket(), others);
	}

	private static ServerSocket createServerSocket() {
		ServerSocket tmp = null;
		try {
			tmp = new ServerSocket(0);
		} catch (IOException e) {
			logger.severe(e.toString());
		}
		return tmp;
	}

	private Map<Long, Socket> bootstrap(long id, Map<Long, InetSocketAddress> others) throws IOException, EntryPeerUnreachableException {
		if (others.size() == 0) {
			// We are alone in this ring, no need to make any connection
			return new HashMap<>();
		} else {
			// Here we are, there are some people in this ring:
			// First we connect to our entry peer (one chosen randomly)
			long entrypeerId = choice(others.keySet());
			InetSocketAddress isa = others.get(entrypeerId);
			Socket tmp = new Socket(isa.getAddress(), isa.getPort());
			Channel c = new SocketChannel(id, -1, tmp);
			// Now we send them a bootstrap message with our id, IP and port
			// and wait for their response with the updated list of peers
			c.sendMessage(new BootstrapMessage(id, 0, getLocalAddress().getHostAddress(), ss.getLocalPort()));
			Message m = c.receiveMessage();
			c.close();
			if (!(m instanceof PeerListMessage))
				throw new EntryPeerUnreachableException();
			PeerListMessage msg = (PeerListMessage) m;

			// The bootstrap message is then going to be forwarded to all
			// the peers inside the ring thanks to the entry peer.

			// After receiving that message they are all going to try to
			// connect to us so let's get ready
			Map<Long, Socket> sockets = new HashMap<>();
			for (int i = 0; i < msg.peers.size(); i++) {
				Socket s = ss.accept();
				// What's your ID kind stranger?
				long peerId = readIdFromSocket(s);
				sockets.put(peerId, s);
			}
			// Okay, all connections have been established, we can return
			return sockets;
		}
	}

	private static <T> T choice(Collection<T> values) {
		int pos = new Random().nextInt(values.size());

		Iterator<T> iter = values.iterator();
		for (int i = 0; i < pos; i++)
			iter.next();
		return iter.next();
	}

	public void loopAccept() {
		while (alive) {
			try {
				Socket s = ss.accept();
				Channel c = new SocketChannel(id, -1, s);

				try {
					Message m = c.receiveMessage();
					if (m instanceof BootstrapMessage) {
						BootstrapMessage bm = (BootstrapMessage) m;
						if (!token.isSpinning()) {
							// If the token is not spinning, this
							// BootstrapMessage will never be processed
							// by the main loop!
							synchronized (this) {
								process(new Pair<>(c, bm));
							}
						} else {
							requests.add(new Pair<>(c, bm));
						}
					} else {
						// discard the request
						c.close();
						s.close();
					}
				} catch (IOException e) {
					logger.fine("Someone failed to join, don't care");
					logger.fine(e.toString());
				}
			} catch (SocketException e) {
				logger.info("Accepting socket was closed, turning off");
			} catch (IOException e) {
				logger.warning(e.toString());
			}
		}
	}

	public void loop() {
		if (channels.size() == 1) {
			try {
				token.stopSpinning();
				multicast.sendTo(this.id, new TokenExchange());
			} catch (InterruptedException e) {
				logger.warning(e.getMessage());
			}
		}
		synchronized (this) {
			alive = true;
			closed = false;
			notify();
		}
		peer.onStart();
		acceptor.start();
		try {
			while (alive) {
				Event event = multicast.next();
				if (event != null) {
					synchronized (this) {
						event.process(this);
					}
				}
			}
		} catch (InterruptedException e) {
			logger.warning(e.toString());
		} finally {
			logger.info("Shutting down");
			try {
				close();
			} catch (IOException e) {
				logger.info(e.toString());
			}
		}
	}

	@Override
	public void process(TokenExchange event) {
		Pair<Channel, BootstrapMessage> pair = null;
		synchronized (requests) {
			if (requests.size() > 0) {
				pair = requests.poll();
			}
		}
		if (pair != null && !channels.contains(pair.y.senderId)) {
			process(pair);
		} else {
			peer.onToken();
		}
	}

	@Override
	public void process(NodeJoining nj) {
		long peerId = nj.bootstrap.senderId;
		Socket s;
		try {
			s = new Socket(nj.bootstrap.address, nj.bootstrap.port);
			writeIdToSocket(s, id);
			multicast.addPeer(peerId, s);
			channels.add(peerId);
		} catch (IOException e) {
			logger.warning(e.toString());
		}
	}

	@Override
	public void process(NodeLeaving nl) {
		if (this.id != nl.id) {
			// When a node leaves the ring, it doesn't pass on
			// the token to the next node. Therefore, if we're
			// that node, we need to regenerate the token by
			// sending it to ourselves.
			// This to avoid deadlocking the whole ring.
			if (findPrev(channels, this.id) == nl.id) {
				logger.fine("I'm (" + id + ") the prev of " + findPrev(channels, this.id));
				if (channels.size() <= 2) {
					try {
						token.stopSpinning();
					} catch (InterruptedException e) {
						logger.warning(e.getMessage());
					}
				}
				multicast.sendTo(this.id, new TokenExchange());
			}
			multicast.removePeer(nl.id);
			channels.remove(nl.id);
			peer.onEvent(new PlayerLeft(nl.id));
		} else {
			shutdown();
		}
	}

	@Override
	public void process(UserEvent event) {
		peer.onEvent(event.event);
	}

	@Override
	public void process(Event event) {
		logger.warning("Got unknown event: " + event);
	}

	private void process(Pair<Channel, BootstrapMessage> pair) {
		Channel c = pair.x;
		BootstrapMessage bm = pair.y;

		c.sendMessage(new PeerListMessage(id, 0, channels));
		c.close();
		if (!token.isSpinning()) {
			try {
				token.startSpinning();
			} catch (InterruptedException e) {
				logger.warning(e.getMessage());
			}
		}
		sendToEveryone(new NodeJoining(bm));
		multicast.sendTo(this.id, new TokenExchange());
	}

	public void attachPeer(Peer n) {
		if (alive || closed)
			throw new IllegalStateException("A peer cannot be attached once the ring has started or is closed!");
		peer = n;
		peer.setToken(token);
	}

	public int getPort() {
		return ss.getLocalPort();
	}

	public synchronized void sendToEveryone(Event event) {
		multicast.broadcast(event);
	}

	public synchronized void sendToNext(Event event) {
		long nextId = findNext(channels, id);
		multicast.sendTo(nextId, event);
	}

	public synchronized void sendToPrev(Event event) {
		long prevId = findPrev(channels, id);
		multicast.sendTo(prevId, event);
	}

	private static long findNext(List<Long> ids, long id) {
		Optional<Long> nextId = ids.stream()
				.filter(peer -> peer > id)
				.reduce(Long::min);
		if (!nextId.isPresent())
			nextId = ids.stream().reduce(Long::min);
		assert nextId.isPresent();
		return nextId.get();
	}

	private static long findPrev(List<Long> ids, long id) {
		Optional<Long> prevId = ids.stream()
				.filter(peer -> peer < id)
				.reduce(Long::max);
		if (!prevId.isPresent())
			prevId = ids.stream().reduce(Long::max);
		assert prevId.isPresent();
		return prevId.get();
	}

	public void start() {
		self.start();
	}

	public void shutdown() {
		alive = false;
	}

	public void close() throws IOException {
		shutdown();
		ss.close();
		try {
			acceptor.join();
			// let's not join ourselves
			if (Thread.currentThread() != self)
				self.join();
		} catch (InterruptedException e) {}
		multicast.close();

		// we need to tell the peers that chose us as
		// entry point that we are leaving!!
		// (can this even happen?)
		for (Pair<Channel, BootstrapMessage> pair : requests) {
			// it doesn't really matter what it contains, it only needs
			// to be different to PeerListMessage
			pair.x.sendMessage(new UnicastMessage(id, 0, new NodeLeaving(id)));
			pair.x.close();
		}

		synchronized (this) {
			closed = true;
			notifyAll();
		}
	}

	public synchronized void waitUntilReady() throws InterruptedException {
		// a spurious wake-ups can happen
		while (!alive)
			wait();
	}

	public synchronized void waitUntilClosed() throws InterruptedException {
		// a spurious wake-ups can happen
		while (!closed)
			wait();
	}

	/**
	 * Sends the `id` to the peer at the other end of `sock`. This is needed to
	 * bootstrap the connection.
	 *
	 * @param sock
	 *            - where to write to
	 * @param id
	 *            - what to write
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	private static void writeIdToSocket(Socket sock, long id) throws IOException {
		OutputStream ostream = sock.getOutputStream();
		try {
			ostream.write((Long.toString(id) + "\n").getBytes("UTF-8"));
			ostream.flush();
		} catch (UnsupportedEncodingException e) {
			logger.fine("Really? this machine doesn't support UTF-8?!\n"
					+ "Here's a penny kiddo, go by yourself a real computer.");
		}
	}

	/**
	 * Reads the `id` of the peer at the other end of `sock`. This is needed to
	 * bootstrap the connection.
	 *
	 * @param sock
	 *            - where to read from
	 * @return returns the read `id` or throws exception
	 * @throws IOException
	 */
	private static long readIdFromSocket(Socket sock) throws IOException {
		InputStream istream = sock.getInputStream();
		String peerIdData = "";
		int n;
		while ((n = istream.read()) != -1 && n != '\n') {
			peerIdData += (char) n;
		}
		return Long.parseLong(peerIdData);
	}

	public long getId() {
		return id;
	}

	public int size() {
		return channels.size();
	}
}
