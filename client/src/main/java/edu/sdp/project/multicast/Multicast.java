package edu.sdp.project.multicast;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Logger;

import edu.sdp.project.channels.BroadcastChannel;
import edu.sdp.project.channels.Channel;
import edu.sdp.project.channels.LoopbackChannel;
import edu.sdp.project.channels.SocketChannel;
import edu.sdp.project.multicast.messages.AcknowledgeMessage;
import edu.sdp.project.multicast.messages.EventMessage;
import edu.sdp.project.multicast.messages.Message;
import edu.sdp.project.multicast.messages.MessageProcessor;
import edu.sdp.project.multicast.messages.MulticastMessage;
import edu.sdp.project.multicast.messages.Poison;
import edu.sdp.project.multicast.messages.UnicastMessage;
import edu.sdp.project.ring.events.Event;
import edu.sdp.project.utils.BlockingQueueWrapper;
import edu.sdp.project.utils.Pair;
import edu.sdp.project.utils.SynchronizedQueue;

import static java.util.Collections.emptySet;

public class Multicast implements Closeable, MessageProcessor {

	private static final Logger logger = Logger.getLogger(Multicast.class.getName());

	private final long id;
	private final Thread self;
	private final BroadcastChannel broadcast;
	private final Map<Long, Channel> channels;
	private Map<Long, Socket> nodes;

	private final BlockingQueue<EventMessage> outputQueue;
	private final BlockingQueue<EventMessage> middleQueue;
	private final BlockingQueue<Message> incomingQueue;
	private final Map<Pair<Long, Long>, Set<Long>> acks;

	private long _nextTimestamp;

	private volatile boolean alive;

	private Message lastMessage = new UnicastMessage(-1, -1, null);

	public Multicast(long id, Map<Long, Socket> nodes) {
		this.id = id;
		this.nodes = nodes;
		this.outputQueue = new BlockingQueueWrapper<>(new PriorityQueue<>());
		this.middleQueue = new BlockingQueueWrapper<>(new PriorityQueue<>());
		this.incomingQueue = new SynchronizedQueue<>();

		this.channels = new HashMap<>();
		this.broadcast = new BroadcastChannel();

		this.self = new Thread(this::loop);
		this.self.setName("Multicast-Ring-" + id);
		this.alive = false;

		this.acks = new HashMap<>();
		this._nextTimestamp = 0;
	}

	public void init() {
		for (Entry<Long, Socket> e : nodes.entrySet()) {
			long peerId = e.getKey();
			addPeer(peerId, e.getValue());
		}
		addPeer(id, new LoopbackChannel(incomingQueue));

		nodes = null; // not needed anymore
		alive = true;
		self.start();
	}

	public synchronized void addPeer(long id, Socket sock) {
		Channel c = new SocketChannel(this.id, id, sock, incomingQueue);
		addPeer(id, c);
	}

	private synchronized void addPeer(long id, Channel c) {
		channels.put(id, c);
		broadcast.addChannel(c);
	}

	public synchronized void removePeer(long id) {
		if (channels.containsKey(id)) {
			Channel c = channels.remove(id);
			broadcast.removeChannel(c);
			c.close();
		}

		// we'll never receive an ack from a dead peer
		// so let's just assume it's been ack'ed from it
		for (Set<Long> acksMissing : acks.values())
			acksMissing.remove(id);
	}

	private void loop() {
		while (alive) {
			try {
				Message m = incomingQueue.take();

				if (m instanceof Poison) {
					/*
					 * Got a poisoned message that means `alive` has been set to
					 * false. Time to close gracefully!
					 */
					continue;
				}
				updateTimestamp(m.timestamp);

				synchronized (this) {
					m.process(this);
					forwardEvents();
				}
			} catch (InterruptedException e) {
				logger.warning(e.toString());
			}
		}
	}

	@Override
	public void process(MulticastMessage message) {
		Pair<Long, Long> key = new Pair<>(message.senderId, message.timestamp);
		try {
			broadcast(ack(message));

			if (!acks.containsKey(key)) {
				Set<Long> acksMissing = new HashSet<>(channels.keySet());
				acksMissing.remove(message.senderId);
				acks.put(key, acksMissing);
			}
			middleQueue.put(message);
		} catch (InterruptedException e) {
			logger.warning(e.toString());
		}
	}

	@Override
	public void process(UnicastMessage message) {
		Pair<Long, Long> key = new Pair<>(message.senderId, message.timestamp);
		try {
			// A unicast message isn't ack'ed by anyone
			// so we pretend that it's already been fully ack'ed
			acks.put(key, emptySet());
			middleQueue.put(message);
		} catch (InterruptedException e) {
			logger.warning(e.toString());
		}
	}

	@Override
	public void process(AcknowledgeMessage message) {
		Pair<Long, Long> key = new Pair<>(message.ackedSenderId, message.ackedTimestamp);
		Set<Long> acksMissing;
		if (acks.containsKey(key)) {
			acksMissing = acks.get(key);
		} else {
			// we got an ack before the message that it's ack'ing
			acksMissing = new HashSet<>(channels.keySet());
		}
		acksMissing.remove(message.senderId);
		acks.put(key, acksMissing);
	}

	private void forwardEvents() {
		Message top;
		try {
			while ((top = middleQueue.peek()) != null) {
				Pair<Long, Long> topKey = new Pair<>(top.senderId, top.timestamp);
				Set<Long> acksMissing = acks.get(topKey);

				assert acksMissing != null;

				if (acksMissing.isEmpty()) {
					acks.remove(topKey);
					outputQueue.put(middleQueue.take());
				} else {
					break;
				}
			}
		} catch (InterruptedException e) {
			logger.warning(e.toString());
		}
	}

	private AcknowledgeMessage ack(Message message) {
		return new AcknowledgeMessage(id, nextTimestamp(), message);
	}

	public synchronized void broadcast(Event event) {
		broadcast(new MulticastMessage(id, nextTimestamp(), event));
	}

	private synchronized void broadcast(Message msg) {
		broadcast.sendMessage(msg);
	}

	public synchronized void sendTo(long id, Event event) {
		sendTo(id, new UnicastMessage(this.id, nextTimestamp(), event));
	}

	private synchronized void sendTo(long id, Message msg) {
		channels.get(id).sendMessage(msg);
	}

	private synchronized long nextTimestamp() {
		return _nextTimestamp++;
	}

	private synchronized void updateTimestamp(long timestamp) {
		if (timestamp >= _nextTimestamp)
			_nextTimestamp = timestamp + 1;
	}

	public synchronized List<Long> getChannelList() {
		return new ArrayList<>(channels.keySet());
	}

	public Event next() throws InterruptedException {
		EventMessage msg = outputQueue.take();
		if (msg instanceof MulticastMessage) {
			synchronized (this) {
				if (!(lastMessage.compareTo(msg) < 0)) {
					String message = "Got a message from the past, last: " + lastMessage + ", curr: " + msg;
					logger.severe(message);
					assert lastMessage.compareTo(msg) < 0 : message;
				}
				lastMessage = msg;
			}
		}
		return msg.event;
	}

	@Override
	public void close() throws IOException {
		alive = false;
		try {
			incomingQueue.put(new Poison());
			self.join();
		} catch (InterruptedException e) {
			logger.warning(e.toString());
		}
		for (Channel c : channels.values())
			c.close();
	}

}
