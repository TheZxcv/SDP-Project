package edu.sdp.project.multicast.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PeerListMessage extends Message {

	public final List<Long> peers;

	@JsonCreator
	public PeerListMessage(@JsonProperty("id") long senderId, @JsonProperty("timestamp") long timestamp,
						   @JsonProperty("peers") List<Long> peers) {
		super(senderId, timestamp);
		this.peers = Collections.unmodifiableList(new ArrayList<>(peers));
	}

	@Override
	public void process(MessageProcessor processor) {
		/* should never be processed */
		;
	}

	@Override
	public String toString() {
		return "PeerListMessage [peers=" + peers + "]";
	}
}
