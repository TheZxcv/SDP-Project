package edu.sdp.project.multicast.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AcknowledgeMessage extends Message {

	@JsonProperty("acked-senderId")
	public final long ackedSenderId;
	@JsonProperty("acked-timestamp")
	public final long ackedTimestamp;

	@JsonCreator
	public AcknowledgeMessage(@JsonProperty("id") long senderId, @JsonProperty("timestamp") long timestamp,
			@JsonProperty("acked-senderId") long ackedSenderId, @JsonProperty("acked-timestamp") long ackedTimestamp) {
		super(senderId, timestamp);
		this.ackedSenderId = ackedSenderId;
		this.ackedTimestamp = ackedTimestamp;
	}

	public AcknowledgeMessage(long senderId, long timestamp, Message message) {
		this(senderId, timestamp, message.senderId, message.timestamp);
	}

	@Override
	public void process(MessageProcessor processor) {
		processor.process(this);
	}

	@Override
	public String toString() {
		return "Acknowledge [id=" + senderId + ",timestamp=" + timestamp + "," +
				"acked-senderId=" + ackedSenderId + ",acked-timestamp" + ackedTimestamp + "]";
	}

}
