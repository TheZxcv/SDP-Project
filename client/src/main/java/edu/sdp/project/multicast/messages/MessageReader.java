package edu.sdp.project.multicast.messages;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageReader {

	private final InputStream input;
	private JsonParser parser;

	public MessageReader(InputStream input) {
		this.input = input;
		this.parser = null;
	}

	public Message read() throws IOException {
		if (parser == null)
			parser = new ObjectMapper().getFactory().createParser(input);
		return parser.readValueAs(Message.class);
	}
}
