package edu.sdp.project.multicast.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.sdp.project.ring.events.Event;

public abstract class EventMessage extends Message {

	public final Event event;

	@JsonCreator
	public EventMessage(@JsonProperty("id") long senderId, @JsonProperty("timestamp") long timestamp,
			@JsonProperty("event") Event event) {
		super(senderId, timestamp);
		this.event = event;
	}

	public abstract void process(MessageProcessor processor);

	@Override
	public String toString() {
		return "EventMessage [id=" + senderId + ",timestamp=" + timestamp + ",event=" + event + "]";
	}
}
