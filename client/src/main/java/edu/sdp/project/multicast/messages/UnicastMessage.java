package edu.sdp.project.multicast.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import edu.sdp.project.ring.events.Event;

public class UnicastMessage extends EventMessage {

	@JsonCreator
	public UnicastMessage(@JsonProperty("id") long senderId, @JsonProperty("timestamp") long timestamp,
			@JsonProperty("event") Event event) {
		super(senderId, timestamp, event);
	}

	@Override
	public void process(MessageProcessor processor) {
		processor.process(this);
	}

	@Override
	public String toString() {
		return "UnicastMessage [id=" + senderId + ",timestamp=" + timestamp + ",event=" + event + "]";
	}
}
