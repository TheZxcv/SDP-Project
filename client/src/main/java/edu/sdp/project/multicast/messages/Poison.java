package edu.sdp.project.multicast.messages;

public class Poison extends Message {

	public Poison() {
		super(-1, -1);
	}

	@Override
	public String toString() {
		return "Poison []";
	}

	@Override
	public void process(MessageProcessor processor) {
		;
	}
}
