package edu.sdp.project.multicast.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BootstrapMessage extends Message {

	public final String address;
	public final int port;

	@JsonCreator
	public BootstrapMessage(@JsonProperty("id") long senderId, @JsonProperty("timestamp") long timestamp,
			@JsonProperty("address") String address, @JsonProperty("port") int port) {
		super(senderId, timestamp);
		this.address = address;
		this.port = port;
	}

	@Override
	public void process(MessageProcessor processor) {
		/* should never be processed */
		;
	}

	@Override
	public String toString() {
		return "BootstrapMessage [id=" + senderId + ",timestamp=" + timestamp + ",address=" + address + ", port="
				+ port + "]";
	}

}
