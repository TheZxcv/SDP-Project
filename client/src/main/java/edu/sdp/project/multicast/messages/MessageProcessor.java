package edu.sdp.project.multicast.messages;

public interface MessageProcessor {
	public void process(MulticastMessage message);

	public void process(UnicastMessage message);

	public void process(AcknowledgeMessage acknowledge);
}
