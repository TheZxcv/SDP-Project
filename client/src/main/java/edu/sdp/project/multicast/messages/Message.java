package edu.sdp.project.multicast.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value=UnicastMessage.class, name="unicast"),
	@JsonSubTypes.Type(value=MulticastMessage.class, name="multicast"),
	@JsonSubTypes.Type(value=AcknowledgeMessage.class, name="ack"),
	@JsonSubTypes.Type(value=BootstrapMessage.class, name="bootstrap"),
	@JsonSubTypes.Type(value=PeerListMessage.class, name="peer-list")
})

public abstract class Message implements Comparable<Message> {

	public final long senderId;
	public final long timestamp;

	@JsonCreator
	public Message(@JsonProperty("id") long senderId, @JsonProperty("timestamp") long timestamp) {
		this.senderId = senderId;
		this.timestamp = timestamp;
	}

	public abstract void process(MessageProcessor processor);

	@Override
	public String toString() {
		return "Message [id=" + senderId + ",timestamp=" + timestamp + "]";
	}

	@Override
	public int compareTo(Message that) {
		if (this.timestamp > that.timestamp)
			return 1;
		else if (this.timestamp < that.timestamp)
			return -1;
		else // this.timestamp == that.timestamp
			return Long.signum(this.senderId - that.senderId);
	}
}
