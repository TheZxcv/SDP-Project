package edu.sdp.project.multicast.messages;

import java.io.IOException;
import java.io.OutputStream;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageWriter {

	private final OutputStream output;
	private JsonGenerator writer;

	public MessageWriter(OutputStream output) {
		this.output = output;
		this.writer = null;
	}

	public void write(Message msg) throws IOException {
		if (writer == null)
			writer = new ObjectMapper().getFactory().createGenerator(output);
		writer.writeObject(msg);
	}

	public void flush() throws IOException {
		if (writer != null)
			writer.flush();
	}

	public void close() throws IOException {
		writer.close();
	}
}
