package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.sdp.project.utils.Point;

public class LocatedAt implements GameEvent {

	public final Point pos;

	@JsonCreator
	public LocatedAt(@JsonProperty("pos") Point pos) {
		this.pos = pos;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof LocatedAt) {
			LocatedAt that = (LocatedAt) other;
			return this.pos.equals(that.pos);
		}
		return false;
	}

	@Override
	public String toString() {
		return "LocatedAt [pos=" + pos + "]";
	}
}
