package edu.sdp.project.game.handlers;

import edu.sdp.project.cli.Console;
import edu.sdp.project.game.Bomb;
import edu.sdp.project.game.PlayerState;
import edu.sdp.project.game.actions.GameAction;
import edu.sdp.project.game.events.BombDropped;
import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.ExplosionHandlingRequest;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.LocatedAt;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.game.events.PlayerJoined;
import edu.sdp.project.game.events.PlayerLeft;
import edu.sdp.project.game.events.Won;
import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;
import edu.sdp.project.ring.Peer;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Logger;

public class GameState extends GameHandler {

	private static final Logger logger = Logger.getLogger(GameState.class.getName());

	protected final Game game;
	protected final PlayerState playerState;
	protected final Peer peer;
	protected Console console = Console.getDefault();

	private final Queue<BombExplosion> queuedExplosions;
	private final Set<BombExplosion> sentExplosions;

	private boolean turnHandlingDelayed;
	private GameHandler handler;

	private GameAction action;
	private final Object lock = new Object();

	private volatile boolean isRunning = true;

	public GameState(Game game, Player player, Peer peer) {
		this(game, new PlayerState(player), peer);
	}

	protected GameState(Game game, PlayerState player, Peer peer) {
		this.game = game;
		this.playerState = player;
		this.peer = peer;
		this.handler = new Spawning(this);
		this.turnHandlingDelayed = false;

		this.queuedExplosions = new LinkedList<>();
		this.sentExplosions = new HashSet<>();

		this.action = null;
	}

	protected void changeHandler(GameHandler handler) {
		this.handler = handler;
	}

	public void setConsole(Console console) {
		this.console = console;
	}

	@Override
	public void handleTurn() {
		if (queuedExplosions.isEmpty()) {
			handler.handleTurn();
		} else {
			turnHandlingDelayed = true;
			do {
				BombExplosion be = queuedExplosions.poll();
				peer.send(be);
				sentExplosions.add(be);
			} while (!queuedExplosions.isEmpty());
		}
		if (!peer.isTokenSpinning())
			peer.unlockToken();
	}

	@Override
	public void handleMove(MoveEvent me) {
		handler.handleMove(me);
	}

	@Override
	public void handleBomb(BombDropped bd) {
		handler.handleBomb(bd);
	}

	@Override
	public void handleRequest(ExplosionHandlingRequest ehr) {
		String username = ehr.username;
		Bomb bomb = ehr.bomb;
		queuedExplosions.offer(new BombExplosion(username, bomb));

		if (!peer.isTokenSpinning()) {
			// The token is not spinning, so we need to make it
			// spin once to deal with the effects of the bomb
			try {
				peer.lockToken();
				peer.spinTokenOnce();
			} catch (IllegalStateException e) {
				assert peer.isTokenSpinning();
				// The token has restarted spinning
				logger.info(e.getMessage());
			} catch (InterruptedException e) {
				logger.warning(e.getMessage());
			}
		}
	}

	@Override
	public void handleExplosion(BombExplosion be) {
		handler.handleExplosion(be);

		if (turnHandlingDelayed) {
			sentExplosions.remove(be);
			if (sentExplosions.isEmpty()) {
				turnHandlingDelayed = false;
				handleTurn();
			}
		} else {
			BombExplosion beQueued = queuedExplosions.poll();
			if (!be.equals(beQueued))
				throw new AssertionError("Bombs handled out of order!?");
		}
	}

	@Override
	public void handleHit(Hit hit) {
		handler.handleHit(hit);
	}

	@Override
	public void handleMiss(Miss miss) {
		handler.handleMiss(miss);
	}

	@Override
	public void handleWinning(Won won) {
		if (playerState.getPlayer().getName().equals(won.winner)) {
			console.println("You won!");
		} else {
			console.println("Player " + won.winner + " won!");
		}
		changeHandler(new Leaving(this));

		// this means we delayed taking care of our turn
		// therefore we are still holding the token
		if (turnHandlingDelayed)
			handleTurn();
	}

	@Override
	public void handlePlayerJoined(PlayerJoined e) {
		if (!playerState.getPlayer().getName().equals(e.username)) {
			console.println("Player " + e.username + " joined the game");
			game.addPlayer(new Player(e.username, null));
			peer.send(new LocatedAt(playerState.getPosition()));
		}
	}

	@Override
	public void handlePlayerLeft(PlayerLeft e) {
		Player p = game.getPlayers().get(e.id);
		game.removePlayer(p.getName());
		console.println("Player " + p.getName() + " left the game");
	}

	@Override
	public void handleLocatedAt(LocatedAt at) {
		handler.handleLocatedAt(at);
	}

	public void addNextAction(GameAction next) throws InterruptedException {
		if (!peer.isTokenSpinning()) {
			try {
				peer.lockToken();
				// no need for synchronization, we are the only thread
				while (action != null) {
					// there is a leftover action to deal with
					peer.spinTokenOnce();
					peer.lockToken();
				}
				action = next;
				peer.spinTokenOnce();
			} catch (IllegalStateException e) {
				assert peer.isTokenSpinning();
				// the token has restarted spinning,
				// we need to submit a move the normal way
				addNextAction(next);
			}
		} else {
			synchronized (lock) {
				// wait for the game to handle a previous action, if there is one
				while (action != null) // a spurious wake-ups can happen
					lock.wait();
				action = next;
				while (action != null) // a spurious wake-ups can happen
					lock.wait();
			}
		}
	}

	protected GameAction consumeAction() {
		GameAction tmp;
		synchronized (lock) {
			tmp = action;
			action = null;
			if (tmp != null)
				lock.notify();
		}
		return tmp;
	}

	public boolean isRunning() {
		return isRunning;
	}

	protected void over() {
		isRunning = false;
		console.interruptConsumer();
		consumeAction(); // unlocks the user input
	}

}
