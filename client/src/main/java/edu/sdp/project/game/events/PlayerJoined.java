package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerJoined implements GameEvent {

	public final String username;

	public PlayerJoined(@JsonProperty("username") String username) {
		this.username = username;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof PlayerJoined) {
			PlayerJoined that = (PlayerJoined) other;
			return this.username.equals(that.username);
		}
		return false;
	}

	@Override
	public String toString() {
		return "PlayerJoined [username='" + username + "']";
	}
}
