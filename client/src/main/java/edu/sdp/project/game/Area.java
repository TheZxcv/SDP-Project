package edu.sdp.project.game;

import edu.sdp.project.utils.Point;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum Area {
	BLUE(2), GREEN(0), RED(1), YELLOW(3);

	public static final Map<Integer, Area> map;

	static {
		Map<Integer, Area> tmp = new HashMap<>();
		for (Area b : Area.values())
			tmp.put(b.key, b);
		map = Collections.unmodifiableMap(tmp);
	}

	private final int key;

	private Area(int key) {
		this.key = key;
	}

	public int getId() {
		return key;
	}

	public boolean contains(Point p, int sideLength) {
		return this == of(p, sideLength);
	}

	public static Area of(int n) {
		return map.get(n % 4);
	}

	public static Area of(Point p, int sideLength) {
		int half = sideLength / 2;
		if (p.y < half) {
			if (p.x < half) {
				return GREEN;
			} else {
				return RED;
			}
		} else {
			if (p.x < half) {
				return BLUE;
			} else {
				return YELLOW;
			}
		}
	}
}
