package edu.sdp.project.game.actions;

public class Leave implements GameAction {

	@Override
	public void apply(GameActionHandler handler) {
		handler.leave();
	}
}
