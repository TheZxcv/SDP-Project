package edu.sdp.project.game;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Bomb {

	private final int id;
	private final Area area;

	public Bomb(@JsonProperty("id") int id, @JsonProperty("area") Area area) {
		this.id = id;
		this.area = area;
	}

	public Area getArea() {
		return area;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, area);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Bomb) {
			Bomb that = (Bomb) other;
			return this.id == that.id && this.area == that.area;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Bomb [" + area + "]";
	}
}
