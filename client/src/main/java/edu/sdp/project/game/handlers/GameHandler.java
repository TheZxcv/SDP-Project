package edu.sdp.project.game.handlers;

import edu.sdp.project.game.Bomb;
import edu.sdp.project.game.actions.GameActionHandler;
import edu.sdp.project.game.events.BombDropped;
import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.ExplosionHandlingRequest;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.LocatedAt;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.game.events.PlayerJoined;
import edu.sdp.project.game.events.PlayerLeft;
import edu.sdp.project.game.events.Won;
import edu.sdp.project.utils.Point;

import java.util.function.Function;

public abstract class GameHandler implements GameActionHandler {

	public void handleTurn() {}
	public void handleMove(MoveEvent me) {}
	public void handleWinning(Won won) {}

	public void handleBomb(BombDropped bd) {}
	public void handleRequest(ExplosionHandlingRequest ehr) {}
	public void handleExplosion(BombExplosion be) {}

	public void handleHit(Hit hit) {}
	public void handleMiss(Miss miss) {}

	public void handlePlayerJoined(PlayerJoined e) {}
	public void handlePlayerLeft(PlayerLeft e) {}
	public void handleLocatedAt(LocatedAt at) {}

	@Override
	public void dropBomb(Bomb bomb) {}
	@Override
	public void move(Function<Point, Point> fn) {}
	@Override
	public void leave() {}
}
