package edu.sdp.project.game.handlers;

import edu.sdp.project.game.PlayerState;
import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.LocatedAt;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.game.events.PlayerJoined;
import edu.sdp.project.model.Game;
import edu.sdp.project.ring.Peer;
import edu.sdp.project.utils.Point;

import java.util.HashSet;
import java.util.Set;

public class Spawning extends GameHandler {

	private final GameState state;
	private final Game game;
	private final PlayerState pState;
	private final Peer peer;
	private final Set<Point> unavailablePoints;

	public Spawning(GameState state) {
		this.state = state;
		this.game = state.game;
		this.pState = state.playerState;
		this.peer = state.peer;

		this.unavailablePoints = new HashSet<>();
	}

	private void spawn() {
		Point p;
		do
			p = Point.randomPoint(game.getSize(), game.getSize());
		while (unavailablePoints.contains(p));

		state.console.println("spawned at " + p);
		pState.setPosition(p);
		state.changeHandler(new Playing(state));
		peer.passToken();
	}

	@Override
	public void handleTurn() {
		if (game.getPlayers().size() == 1)
			spawn();
		else
			peer.send(new PlayerJoined(pState.getPlayer().getName()));
	}

	@Override
	public void handleLocatedAt(LocatedAt at) {
		unavailablePoints.add(at.pos);

		// all players have replied with their position
		if (unavailablePoints.size() == game.getPlayers().size() - 1) {
			spawn();
		}
	}

	@Override
	public void handleMove(MoveEvent me) {
		// cannot be hit while waiting to spawn
		peer.send(new Miss(me.username, Hit.How.MELEE));
	}

	@Override
	public void handleExplosion(BombExplosion be) {
		// cannot be hit while waiting to spawn
		peer.send(new Miss(be.username, Hit.How.EXPLOSION, be.bomb.getId()));
	}
}
