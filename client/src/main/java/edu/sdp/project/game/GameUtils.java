package edu.sdp.project.game;

import edu.sdp.project.cli.Console;
import edu.sdp.project.client.GamesClient;
import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;
import edu.sdp.project.ring.EntryPeerUnreachableException;
import edu.sdp.project.ring.Ring;
import edu.sdp.project.ring.RingImpl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.Map;
import java.util.stream.Collectors;

public class GameUtils {

	private static final int JOIN_GAME_RETRIES = 3;

	private GameUtils() {}

	private static Map<Long, InetSocketAddress> trasformPlayersMap(Map<Long, Player> players) {
		return players.entrySet().stream()
				.filter((e) -> e.getValue().isReady())
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						e -> e.getValue().getSocketAddress())
				);
	}

	public static GameLoop joinGame(GamesClient client, Game game, String username, ServerSocket ss) {
		String gameName = game.getName();

		for (int i = 0; i < JOIN_GAME_RETRIES; i++) {
			// if `game` has been null'ed, we fetch it again
			if (game == null)
				game = client.getGame(gameName);

			try {
				long id = game.findPlayerId(username);
				// try to join the ring
				Ring ring = new RingImpl(id, ss, trasformPlayersMap(game.getPlayers()));
				return new GameLoop(username, ring, game, client);
			} catch (EntryPeerUnreachableException e) {
				// we failed to join the ring, the super peer might have left
				// let's try again
				Console.getDefault().println("Unable to connect to the game, retrying...");
				// we null game to cause a re-fetch
				game = null;
			}
		}

		// we couldn't join the game; we inform the server
		// and remove ourselves from the game's players list
		client.leaveGame(gameName, username);
		throw new IllegalStateException("Unable to join game!");
	}

	public static GameLoop joinGame(GamesClient client, Game game, String username) throws IOException {
		return joinGame(client, game, username, new ServerSocket(0));
	}
}
