package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value=BombDropped.class, name="bomb-dropped"),
	@JsonSubTypes.Type(value=BombExplosion.class, name="bomb-explosion"),
	@JsonSubTypes.Type(value=ExplosionHandlingRequest.class, name="explosion-handling"),
	@JsonSubTypes.Type(value=MoveEvent.class, name="move"),
	@JsonSubTypes.Type(value=LocatedAt.class, name="located-at"),
	@JsonSubTypes.Type(value=PlayerJoined.class, name="player-joined"),
	@JsonSubTypes.Type(value=PlayerLeft.class, name="player-left"),
	@JsonSubTypes.Type(value=Miss.class, name="miss"),
	@JsonSubTypes.Type(value=Hit.class, name="hit"),
	@JsonSubTypes.Type(value=Won.class, name="won")
})

public interface GameEvent {

}
