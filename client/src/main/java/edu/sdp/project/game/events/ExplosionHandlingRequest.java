package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.sdp.project.game.Bomb;

public class ExplosionHandlingRequest implements GameEvent {

	public final String username;
	public final Bomb bomb;

	@JsonCreator
	public ExplosionHandlingRequest(@JsonProperty("username") String username, @JsonProperty("bomb") Bomb bomb) {
		this.username = username;
		this.bomb = bomb;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof ExplosionHandlingRequest) {
			ExplosionHandlingRequest that = (ExplosionHandlingRequest) other;
			return this.username.equals(that.username) && this.bomb.equals(that.bomb);
		}
		return false;
	}

	@Override
	public String toString() {
		return "BombExplosion [username='" + username + "',bomb=" + bomb + "]";
	}
}
