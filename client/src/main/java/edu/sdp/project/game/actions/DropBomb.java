package edu.sdp.project.game.actions;

import edu.sdp.project.game.Bomb;

public class DropBomb implements GameAction {

	private final Bomb bomb;

	public DropBomb(Bomb bomb) {
		if (bomb == null)
			throw new IllegalArgumentException("`bomb` must be not null");
		this.bomb = bomb;
	}

	@Override
	public void apply(GameActionHandler handler) {
		handler.dropBomb(bomb);
	}
}
