package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Hit implements GameEvent {

	public enum How {
		MELEE, EXPLOSION
	}

	public final String killedBy;
	public final How how;
	public final int bombId;

	public Hit(@JsonProperty("killedBy") String killedBy, @JsonProperty("how") How how,
			   @JsonProperty("bombId") int bombId) {
		this.killedBy = killedBy;
		this.how = how;
		this.bombId = bombId;
	}

	public Hit(String killedBy, How how) {
		this(killedBy, how, -1);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Hit) {
			Hit that = (Hit) other;
			if (!this.killedBy.equals(that.killedBy))
				return false;
			if (this.how != that.how)
				return false;
			return this.bombId == that.bombId;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Hit [killedBy='" + killedBy + "',how=" + how + ",bombId=" + bombId + "]";
	}
}
