package edu.sdp.project.game.handlers;

import edu.sdp.project.game.Area;
import edu.sdp.project.game.Bomb;
import edu.sdp.project.game.PlayerState;
import edu.sdp.project.game.actions.GameAction;
import edu.sdp.project.game.events.BombDropped;
import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.ExplosionHandlingRequest;
import edu.sdp.project.game.events.GameEvent;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.game.events.Won;
import edu.sdp.project.model.Game;
import edu.sdp.project.ring.Peer;
import edu.sdp.project.utils.Pair;
import edu.sdp.project.utils.Point;
import edu.sdp.project.utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Playing extends GameHandler {

	private static final int MAX_POINTS_FROM_EXPLOSION = 3;
	private static final long BOMB_EXPLOSION_DELAY = 5000;

	private final GameState state;
	private final Game game;
	private final PlayerState pState;
	private final Peer peer;

	private final Map<Integer, Pair<Integer, Integer>> bombHitAndMiss;

	private int responseCnt;
	private boolean myTurn;

	public Playing(GameState state) {
		this.state = state;
		this.game = state.game;
		this.pState = state.playerState;
		this.peer = state.peer;

		this.bombHitAndMiss = new HashMap<>();

		this.myTurn = false;
		this.responseCnt = -1;
	}

	private void updateResponseCounter() {
		assert myTurn == (responseCnt != -1);
		if (myTurn && responseCnt != -1) {
			responseCnt++;
			if (responseCnt == game.getPlayers().size()) {
				responseCnt = -1;
				tryToEndTurn();
			}
		}
	}

	private void updateBombResponseCounter(int bombId, boolean hit) {
		Pair<Integer, Integer> pair = bombHitAndMiss.get(bombId);
		if (hit) {
			pair = new Pair<>(pair.x + 1, pair.y);
			// add points only for the first `MAX_POINTS_FROM_EXPLOSION` kills
			if (pair.x <= MAX_POINTS_FROM_EXPLOSION)
				addOnePoint();
		} else {
			pair = new Pair<>(pair.x, pair.y + 1);
		}

		if (pair.x + pair.y == game.getPlayers().size()) {
			// everyone has replied
			bombHitAndMiss.remove(bombId);
			tryToEndTurn();
		} else {
			bombHitAndMiss.put(bombId, pair);
		}
	}

	private void addOnePoint() {
		pState.addPoints(1);
		if (pState.getPoints() == game.getTarget()) {
			pState.win();
			peer.send(new Won(pState.getPlayer().getName()));
		}
	}

	private void tryToEndTurn() {
		if (myTurn && responseCnt == -1 && bombHitAndMiss.isEmpty())
			endTurn();
	}

	private void endTurn() {
		myTurn = false;
		peer.passToken();
	}

	private void playerDied() {
		state.console.println("You died");

		pState.die();
		state.changeHandler(new Leaving(state));
		if (myTurn)
			endTurn();
	}

	@Override
	public void handleTurn() {
		myTurn = true;

		GameAction action = state.consumeAction();
		if (action != null) {
			action.apply(this);
		} else {
			tryToEndTurn();
		}
	}

	@Override
	public void move(Function<Point, Point> fn) {
		Point curr = pState.getPosition();
		Point next = fn.apply(curr);
		if (isValidPosition(next)) {
			responseCnt = 0;
			peer.send(new MoveEvent(pState.getPlayer().getName(), next));
		} else {
			tryToEndTurn();
		}
	}

	private boolean isValidPosition(Point pos) {
		if (pos.x < 0 || pos.x >= game.getSize())
			return false;
		return !(pos.y < 0 || pos.y >= game.getSize());
	}

	@Override
	public void dropBomb(Bomb bomb) {
		peer.send(new BombDropped(pState.getPlayer().getName(), bomb));
		tryToEndTurn();
	}

	@Override
	public void leave() {
		peer.leave();
		state.over();
		state.changeHandler(new Leaving(state));
	}

	@Override
	public void handleMove(MoveEvent me) {
		state.console.println(me.username + " moved to " + me.pos);

		if (isItMe(me.username)) {
			pState.setPosition(me.pos);
			updateResponseCounter();
		} else {
			if (pState.getPosition().equals(me.pos)) {
				peer.send(new Hit(me.username, Hit.How.MELEE));
				playerDied();
			} else {
				peer.send(new Miss(me.username, Hit.How.MELEE));
			}
		}
	}

	@Override
	public void handleBomb(BombDropped bd) {
		state.console.println(bd.username + " has dropped a " + bd.bomb + " bomb!!!");

		if (isItMe(bd.username)) {
			GameEvent e = new ExplosionHandlingRequest(bd.username, bd.bomb);
			Thread t = new Thread(()
					-> {
				Utils.sleepWithoutHassle(BOMB_EXPLOSION_DELAY);
				peer.send(e);
			});
			t.start();
		}
	}

	@Override
	public void handleExplosion(BombExplosion be) {
		state.console.println(be.username + "'s " + be.bomb + " bomb exploded!");

		// it's mine, we need to be ready to deal with Hits and Misses
		if (isItMe(be.username)) {
			bombHitAndMiss.put(be.bomb.getId(), new Pair<>(0, 0));
		}

		Area area = be.bomb.getArea();
		Point pos = pState.getPosition();
		int side = game.getSize();
		if (area.contains(pos, side)) {
			peer.send(new Hit(be.username, Hit.How.EXPLOSION, be.bomb.getId()));
			playerDied();
		} else {
			peer.send(new Miss(be.username, Hit.How.EXPLOSION, be.bomb.getId()));
		}
	}

	@Override
	public void handleHit(Hit hit) {
		if (isItMe(hit.killedBy)) {
			if (hit.how == Hit.How.MELEE) {
				addOnePoint();
				updateResponseCounter();
			} else {
				updateBombResponseCounter(hit.bombId, true);
			}
		}
	}

	@Override
	public void handleMiss(Miss miss) {
		if (isItMe(miss.missedBy)) {
			if (miss.how == Hit.How.MELEE)
				updateResponseCounter();
			else if (miss.how == Hit.How.EXPLOSION)
				updateBombResponseCounter(miss.bombId, false);
		}
	}

	private boolean isItMe(String username) {
		return pState.getPlayer().getName().equals(username);
	}
}
