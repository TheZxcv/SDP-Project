package edu.sdp.project.game.actions;

public interface GameAction {
	public void apply(GameActionHandler handler);
}
