package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Miss implements GameEvent {

	public final String missedBy;
	public final Hit.How how;
	public final int bombId;

	public Miss(@JsonProperty("missedBy") String missedBy, @JsonProperty("how") Hit.How how,
				@JsonProperty("bombId") int bombId) {
		this.missedBy = missedBy;
		this.how = how;
		this.bombId = bombId;
	}

	public Miss(String missedBy, Hit.How how) {
		this(missedBy, how, -1);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Miss) {
			Miss that = (Miss) other;
			if (!this.missedBy.equals(that.missedBy))
				return false;
			if (this.how != that.how)
				return false;
			return this.bombId == that.bombId;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Miss [missedBy='" + missedBy + "'how=" + how + ",bombId=" + bombId + "]";
	}
}
