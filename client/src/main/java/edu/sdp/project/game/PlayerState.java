package edu.sdp.project.game;

import edu.sdp.project.model.Player;
import edu.sdp.project.utils.Point;

public class PlayerState {

	private final Player player;
	private Point pos;
	private int points;
	private boolean isAlive;
	private boolean hasWon;

	public PlayerState(Player player) {
		this.player = player;
		this.pos = null;
		this.points = 0;
		this.isAlive = true;
		this.hasWon = false;
	}

	public Player getPlayer() {
		return player;
	}

	public Point getPosition() {
		return pos;
	}

	public void setPosition(Point p) {
		this.pos = p;
	}

	public int getPoints() {
		return points;
	}

	public void addPoints(int delta) {
		points += delta;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void die() {
		isAlive = false;
	}

	public boolean hasWon() {
		return hasWon;
	}

	public void win() {
		this.hasWon = true;
	}

}
