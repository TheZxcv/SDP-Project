package edu.sdp.project.game.handlers;

import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.ring.Peer;

public class Leaving extends GameHandler {
	private final GameState state;
	private final Peer peer;

	public Leaving(GameState state) {
		this.state = state;
		this.peer = state.peer;
	}

	@Override
	public void handleTurn() {
		peer.leave();
		state.over();
	}

	@Override
	public void handleMove(MoveEvent me) {
		// cannot be hit while dead
		peer.send(new Miss(me.username, Hit.How.MELEE));
	}

	@Override
	public void handleExplosion(BombExplosion be) {
		// cannot be hit while dead
		peer.send(new Miss(be.username, Hit.How.EXPLOSION, be.bomb.getId()));
	}
}
