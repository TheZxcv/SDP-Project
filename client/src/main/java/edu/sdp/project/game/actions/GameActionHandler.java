package edu.sdp.project.game.actions;

import edu.sdp.project.game.Bomb;
import edu.sdp.project.utils.Point;

import java.util.function.Function;

public interface GameActionHandler {
	public void dropBomb(Bomb bomb);

	public void move(Function<Point, Point> fn);

	public void leave();
}
