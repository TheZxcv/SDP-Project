package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlayerLeft implements GameEvent {

	public final long id;

	public PlayerLeft(@JsonProperty("id") long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof PlayerLeft) {
			PlayerLeft that = (PlayerLeft) other;
			return this.id == that.id;
		}
		return false;
	}

	@Override
	public String toString() {
		return "PlayerLeft [id='" + id + "']";
	}
}
