package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Won implements GameEvent {

	public final String winner;

	public Won(@JsonProperty("winner") String winner) {
		this.winner = winner;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Won) {
			Won that = (Won) other;
			return this.winner.equals(that.winner);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Won [username='" + winner + "']";
	}
}
