package edu.sdp.project.game.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import edu.sdp.project.utils.Point;

public class MoveEvent implements GameEvent {

	public final String username;
	public final Point pos;

	@JsonCreator
	public MoveEvent(@JsonProperty("username") String username, @JsonProperty("pos") Point pos) {
		this.username = username;
		this.pos = pos;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof MoveEvent) {
			MoveEvent that = (MoveEvent) other;
			if (!this.username.equals(that.username))
				return false;
			return this.pos.equals(that.pos);
		}
		return false;
	}

	@Override
	public String toString() {
		return "MoveEvent [username='" + username + "',pos=" + pos + "]";
	}
}
