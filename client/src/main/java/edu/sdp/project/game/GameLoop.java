package edu.sdp.project.game;

import edu.sdp.project.cli.Console;
import edu.sdp.project.cli.InterruptibleConsoleReader;
import edu.sdp.project.client.GamesClient;
import edu.sdp.project.game.actions.DropBomb;
import edu.sdp.project.game.actions.Leave;
import edu.sdp.project.game.actions.Movement;
import edu.sdp.project.game.actions.Movement.Direction;
import edu.sdp.project.game.events.BombDropped;
import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.ExplosionHandlingRequest;
import edu.sdp.project.game.events.GameEvent;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.LocatedAt;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.game.events.PlayerJoined;
import edu.sdp.project.game.events.PlayerLeft;
import edu.sdp.project.game.events.Won;
import edu.sdp.project.game.handlers.GameState;
import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;
import edu.sdp.project.ring.Peer;
import edu.sdp.project.ring.Ring;
import edu.sdp.project.sensors.BombFinder;
import edu.sdp.project.utils.SynchronizedQueue;

import java.util.concurrent.BlockingQueue;

public class GameLoop extends Peer implements Runnable {

	private final long id;
	private final String username;
	private final Game game;
	private final GamesClient client;
	private final Ring ring;
	private final BombFinder finder;
	private final BlockingQueue<Bomb> bombs;
	private final GameState state;

	private Console console = Console.getDefault();

	public GameLoop(String username, Ring ring, Game game, GamesClient client) {
		super(ring);
		this.id = game.findPlayerId(username);
		this.username = username;
		this.game = game;
		this.client = client;
		this.ring = ring;
		this.bombs = new SynchronizedQueue<>();
		this.finder = new BombFinder(bombs);
		this.state = new GameState(game, game.getPlayers().get(id), this);
	}

	public void setConsole(Console console) {
		this.console = console;
		state.setConsole(console);
	}

	@Override
	public void run() {
		try {
			ring.start();
			ring.waitUntilReady();
			Player me = game.getPlayers().get(id);
			me.setReady();
			client.ready(game.getName(), me);
			finder.start();

			String line;
			boolean alive = true;
			while (state.isRunning() && alive && (line = console.read()) != null) {
				switch (line) {
					case "leave":
						alive = false;
						break;
					case "up":
					case "down":
					case "left":
					case "right":
						state.addNextAction(new Movement(Direction.from(line)));
						break;
					case "bomb":
						Bomb b = bombs.peek();
						if (b != null)
							state.addNextAction(new DropBomb(bombs.take()));
						else
							console.println("no bombs available");
						break;
					default:
						console.println("unrecognized command");
						break;
				}
			}
			console.println("Closing game");
			if (state.isRunning())
				state.addNextAction(new Leave());
			client.leaveGame(game.getName(), username);

			ring.waitUntilClosed();
			finder.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {

	}

	@Override
	public void onEvent(GameEvent event) {
		if (event instanceof BombExplosion)
			state.handleExplosion((BombExplosion) event);
		else if (event instanceof Hit)
			state.handleHit((Hit) event);
		else if (event instanceof BombDropped)
			state.handleBomb((BombDropped) event);
		else if (event instanceof ExplosionHandlingRequest)
			state.handleRequest((ExplosionHandlingRequest) event);
		else if (event instanceof LocatedAt)
			state.handleLocatedAt((LocatedAt) event);
		else if (event instanceof Miss)
			state.handleMiss((Miss) event);
		else if (event instanceof MoveEvent)
			state.handleMove((MoveEvent) event);
		else if (event instanceof PlayerJoined)
			state.handlePlayerJoined((PlayerJoined) event);
		else if (event instanceof PlayerLeft)
			state.handlePlayerLeft((PlayerLeft) event);
		else if (event instanceof Won)
			state.handleWinning((Won) event);
	}

	@Override
	public void onToken() {
		state.handleTurn();
	}
}
