package edu.sdp.project.game.actions;

import edu.sdp.project.utils.Point;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Movement implements GameAction {

	public enum Direction {
		UP((p) -> new Point(p.x, p.y - 1)),
		DOWN((p) -> new Point(p.x, p.y + 1)),
		LEFT((p) -> new Point(p.x - 1, p.y)),
		RIGHT((p) -> new Point(p.x + 1, p.y));

		public static final Map<String, Direction> map;

		static {
			Map<String, Direction> tmp = new HashMap<>();
			for (Direction d : Direction.values())
				tmp.put(d.name().toLowerCase(), d);
			map = Collections.unmodifiableMap(tmp);
		}

		private Function<Point, Point> fn;

		private Direction(Function<Point, Point> fn) {
			this.fn = fn;
		}

		public static Direction from(String input) {
			return map.get(input.toLowerCase());
		}
	}

	private final Direction dir;

	public Movement(Direction dir) {
		this.dir = dir;
	}

	@Override
	public void apply(GameActionHandler handler) {
		handler.move(dir.fn);
	}
}
