package edu.sdp.project.client;

import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status;

public class GamesClient {
	private static final String SERVER_URI_FORMAT = "http://%s:%d/server";

	private final String uri;
	private final Client client;
	private final WebTarget mainTarget;

	public GamesClient(String server, int port) {
		this.client = ClientBuilder.newClient();
		this.uri = String.format(SERVER_URI_FORMAT, server, port);
		this.mainTarget = client.target(uri).path("games");
	}

	public List<String> getAllGames() {
		Response resp = mainTarget.request(MediaType.APPLICATION_JSON).get();
		return resp.readEntity(new GenericType<List<String>>() {});
	}

	public Game getGame(String name) {
		Response resp = mainTarget
				.path(name)
				.request(MediaType.APPLICATION_JSON)
				.get();
		if (resp.getStatus() == Status.NOT_FOUND.getStatusCode())
			return null;
		return resp.readEntity(Game.class);
	}

	public void createGame(String name, int size, int target) throws IllegalArgumentException {
		Game game = new Game(name, size, target);
		Response resp = mainTarget
				.path(name)
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(game, MediaType.APPLICATION_JSON));
		if (resp.getStatus() == Status.CONFLICT.getStatusCode())
			throw new IllegalArgumentException("there is already a game named `" + name + "`");
		if (resp.getStatus() == Status.BAD_REQUEST.getStatusCode())
			throw new IllegalArgumentException("What happened?");
	}

	public Game joinGame(String game, Player player) throws IllegalArgumentException {
		Response resp = mainTarget
				.path(game)
				.path(player.getName())
				.request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(player, MediaType.APPLICATION_JSON));
		if (resp.getStatus() == Status.NOT_FOUND.getStatusCode())
			throw new IllegalArgumentException("no such game `" + game + "`");
		if (resp.getStatus() == Status.CONFLICT.getStatusCode())
			throw new IllegalArgumentException("there is already a player named `" +
					player.getName() + "` in game `" + game + "`");
		if (resp.getStatus() == Status.BAD_REQUEST.getStatusCode())
			throw new IllegalArgumentException("the game `" + game + "` is full");
		return resp.readEntity(Game.class);
	}

	public void ready(String game, Player player) throws IllegalArgumentException {
		Response resp = mainTarget
				.path(game)
				.path(player.getName())
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(player, MediaType.APPLICATION_JSON));
		if (resp.getStatus() == Status.NOT_FOUND.getStatusCode())
			throw new IllegalArgumentException("no such game `" + game + "`");
		if (resp.getStatus() == Status.BAD_REQUEST.getStatusCode())
			throw new IllegalArgumentException("no player `" + player.getName() + "` in game `" + game + "`");
	}

	public void leaveGame(String game, String playerName) throws IllegalArgumentException {
		Response resp = mainTarget
				.path(game)
				.path(playerName)
				.request(MediaType.APPLICATION_JSON)
				.delete();
		if (resp.getStatus() == Status.NOT_FOUND.getStatusCode())
			throw new IllegalArgumentException("no such game `" + game + "`");
		if (resp.getStatus() == Status.BAD_REQUEST.getStatusCode())
			throw new IllegalArgumentException("there is no player named `" +
					playerName + "` in game `" + game + "`");
	}

	public void removeGame(String game) throws IllegalArgumentException {
		Response resp = mainTarget
				.path(game)
				.request(MediaType.APPLICATION_JSON)
				.delete();
		if (resp.getStatus() == Status.NOT_FOUND.getStatusCode())
			throw new IllegalArgumentException("no such game `" + game + "`");
	}
}
