package edu.sdp.project;

import edu.sdp.project.cli.ClientCLI;
import edu.sdp.project.cli.Console;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) throws Exception {
		ClientCLI cli = new ClientCLI();
		boolean retry = false;
		do {
			try {
				retry = false;
				cli.start();
			} catch (UnknownHostException | ConnectException e) {
				retry = true;
				Console.getDefault().println("Invalid server, try another one...");
				logger.info(e.getMessage());
			} catch (IOException e) {
				Console.getDefault().println("stdin closed!");
				logger.warning(e.getMessage());
			} catch (Exception e) {
				// PANIC!
				e.printStackTrace();
				logger.severe(e.getMessage());
				throw e;
			}
		} while (retry);
	}

}
