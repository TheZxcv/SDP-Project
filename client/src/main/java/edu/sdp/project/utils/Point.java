package edu.sdp.project.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.Random;

public class Point {

	private static final Random rnd = new Random();

	public final int x;
	public final int y;

	@JsonCreator
	public Point(@JsonProperty("x") int x, @JsonProperty("y") int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (other == null)
			return false;

		if (other instanceof Point) {
			Point that = (Point) other;
			return this.x == that.x && this.y == that.y;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ",y=" + y + "]";
	}

	public static Point randomPoint(int xbound, int ybound) {
		return new Point(rnd.nextInt(xbound), rnd.nextInt(ybound));
	}
}
