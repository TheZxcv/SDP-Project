package edu.sdp.project.utils;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public final class Utils {
	private Utils() {}

	public static long sleepWithoutHassle(long millis) {
		long startTime = System.currentTimeMillis();
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new AssertionError(e);
		}
		return System.currentTimeMillis() - startTime;
	}

	public static InetAddress getLocalAddress() throws SocketException {
		Enumeration<NetworkInterface> nics = NetworkInterface.getNetworkInterfaces();
		while (nics.hasMoreElements()) {
			NetworkInterface nic = nics.nextElement();
			if (!nic.isLoopback()) {
				for (InterfaceAddress addr : nic.getInterfaceAddresses())
					if (addr.getAddress() instanceof Inet4Address)
						return addr.getAddress();
			}
		}
		return InetAddress.getLoopbackAddress();
	}
}
