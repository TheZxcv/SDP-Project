package edu.sdp.project.utils;

import java.util.Objects;

public class Pair<X, Y> {

	public final X x;
	public final Y y;

	public Pair(X x, Y y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		} else if (other instanceof Pair) {
			Pair<?, ?> that = (Pair<?, ?>) other;
			return this.x.equals(that.x) && this.y.equals(that.y);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Pair [x=" + x + ", y=" + y + "]";
	}
}
