package edu.sdp.project.utils;

import java.util.LinkedList;

public class SynchronizedQueue<E> extends BlockingQueueWrapper<E> {
	public SynchronizedQueue() {
		super(new LinkedList<>());
	}
}