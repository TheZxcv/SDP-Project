package edu.sdp.project.utils;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class BlockingQueueWrapper<E> extends AbstractQueue<E> implements BlockingQueue<E> {

	private static final Logger logger = Logger.getLogger(BlockingQueueWrapper.class.getName());

	private final Queue<E> wrappee;

	public BlockingQueueWrapper(Queue<E> wrappee) {
		this.wrappee = wrappee;
	}

	/**
	 * Takes and returns the head of this queue or blocks if the queue is empty.
	 *
	 * @return the head of this queue
	 * @throws InterruptedException
	 */
	public E take() throws InterruptedException {
		E e;
		synchronized (wrappee) {
			e = wrappee.poll();
			// a spurious wake-ups can happen
			while (e == null) {
				wrappee.wait();
				e = wrappee.poll();
			}
		}
		return e;
	}

	/**
	 * Inserts the element <i>e</i> into this queue, blocks if necessary for
	 * space to become available.
	 *
	 * @param e
	 *            - the element to add
	 * @throws InterruptedException
	 */
	public void put(E e) throws InterruptedException {
		synchronized (wrappee) {
			if (!offer(e))
				logger.severe("THIS SHOULD NEVER HAPPEN: offer failed on delegate queue");
		}
	}

	@Override
	public E poll() {
		synchronized (wrappee) {
			return wrappee.poll();
		}
	}

	@Override
	public boolean offer(E e) {
		synchronized (wrappee) {
			boolean res = wrappee.offer(e);
			wrappee.notify();
			return res;
		}
	}

	@Override
	public E peek() {
		synchronized (wrappee) {
			return wrappee.peek();
		}
	}

	@Override
	public Iterator<E> iterator() {
		synchronized (wrappee) {
			return new ArrayList<>(wrappee).iterator();
		}
	}

	@Override
	public int size() {
		synchronized (wrappee) {
			return wrappee.size();
		}
	}

	@Override
	public String toString() {
		synchronized (wrappee) {
			return "BlockingQueueWrapper [queue=" + wrappee + "]";
		}
	}

	@Override
	public int remainingCapacity() {
		return Integer.MAX_VALUE;
	}

	@Override
	public int drainTo(Collection<? super E> c) {
		int cnt = 0;
		synchronized (wrappee) {
			E e;
			while ((e = wrappee.poll()) != null) {
				c.add(e);
				cnt++;
			}
		}
		return cnt;
	}

	@Override
	public int drainTo(Collection<? super E> c, int maxElements) {
		int cnt = 0;
		synchronized (wrappee) {
			E e;
			while (cnt < maxElements && (e = wrappee.poll()) != null) {
				c.add(e);
				cnt++;
			}
		}
		return cnt;
	}

	@Override
	public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
		throw new RuntimeException("Not implemented yet");
	}

	@Override
	public E poll(long timeout, TimeUnit unit) throws InterruptedException {
		throw new RuntimeException("Not implemented yet");
	}

}
