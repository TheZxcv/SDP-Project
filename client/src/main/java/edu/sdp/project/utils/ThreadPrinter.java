package edu.sdp.project.utils;

import static java.lang.System.out;

public class ThreadPrinter {

	private ThreadPrinter() {}

	public static <T> void println(T message) {
		out.println(Thread.currentThread().getName() + ": " + message.toString());
	}

	public static <T> void print(T message) {
		out.print(Thread.currentThread().getName() + ": " + message.toString());
	}
}
