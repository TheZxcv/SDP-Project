package edu.sdp.project.multicast;

import edu.sdp.project.ring.events.NodeLeaving;
import edu.sdp.project.utils.Pair;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

public class MulticastTest {
	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testMulticastOrdering() throws Exception {
		List<Multicast> multicasts = new ArrayList<>();
		List<Thread> consumers = new ArrayList<>();
		List<Thread> producers = new ArrayList<>();
		List<List<Long>> results = new ArrayList<>();
		for (long id : Arrays.asList(0, 1, 2)) {
			Multicast m = new Multicast(id, new HashMap<>());
			multicasts.add(m);

			List<Long> res = new ArrayList<>();
			Thread t = makeConsumer(res, m);
			consumers.add(t);
			results.add(res);

			producers.add(makeProducer(id, 3, m));
		}

		Pair<Socket, Socket> p;
		p = createPairOfSockets();
		multicasts.get(0).addPeer(1, p.x);
		multicasts.get(1).addPeer(0, p.y);

		p = createPairOfSockets();
		multicasts.get(0).addPeer(2, p.x);
		multicasts.get(2).addPeer(0, p.y);

		p = createPairOfSockets();
		multicasts.get(1).addPeer(2, p.x);
		multicasts.get(2).addPeer(1, p.y);

		for (int i = 0; i < multicasts.size(); i++) {
			multicasts.get(i).init();
			consumers.get(i).start();
		}

		for (Thread t : producers)
			t.start();

		for (Thread t : producers)
			t.join();

		for (Thread t : consumers)
			t.join();

		for (Multicast m : multicasts)
			m.close();

		for (List<Long> r1 : results) {
			for (List<Long> r2 : results) {
				assertEquals(r1, r2);
			}
		}
	}

	private static Pair<Socket, Socket> createPairOfSockets() throws Exception {
		List<Socket> res = new ArrayList<>(1);
		ServerSocket ss = new ServerSocket(0);
		int port = ss.getLocalPort();
		Thread t = new Thread(() -> {
			try {
				res.add(ss.accept());
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					ss.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
		Socket s = new Socket(ss.getInetAddress(), port);
		t.join();
		return new Pair<>(res.get(0), s);
	}

	private static Thread makeProducer(long mod, long p, Multicast m) {
		return new Thread(
				() -> {
					Random r = new Random();
					for (int i = 1; i < 1000; i++) {
						// abusing this event
						m.broadcast(new NodeLeaving(p * i - mod));
					}
				}
		);
	}

	private static Thread makeConsumer(Collection<Long> c, Multicast m) {
		return new Thread(
				() -> {
					for (int i = 1; i < 1000; i++)
						try {
							c.add(((NodeLeaving) m.next()).id);
						} catch (InterruptedException e) {
							throw new AssertionError(e);
						}
				}
		);
	}
}
