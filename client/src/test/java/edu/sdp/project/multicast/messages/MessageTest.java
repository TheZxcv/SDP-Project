package edu.sdp.project.multicast.messages;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.MapperFeature;
import edu.sdp.project.game.Area;
import edu.sdp.project.game.Bomb;
import edu.sdp.project.game.events.BombDropped;
import edu.sdp.project.ring.events.Event;
import edu.sdp.project.ring.events.UserEvent;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.sdp.project.ring.events.TokenExchange;

public class MessageTest {

	private ObjectMapper mapper;

	@Before
	public void setUp() {
		mapper = new ObjectMapper();
		// deterministic properties order
		mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
	}

	@Test
	public void testTokenExchange() throws Exception {
		String json = "{\"type\":\"unicast\",\"event\":{\"type\":\"token\"},\"timestamp\":0,\"senderId\":0}";
		Message message = mapper.readValue(json, Message.class);
		UnicastMessage expected = new UnicastMessage(0,0, new TokenExchange());

		assertEquals(json, mapper.writeValueAsString(expected));

		assertEquals(message.timestamp, 0);
		assertEquals(UnicastMessage.class, message.getClass());
		EventMessage em = (EventMessage) message;
		assertEquals(TokenExchange.class, em.event.getClass());
	}

	@Test
	public void testAcknowledge() throws Exception {
		String json = "{\"type\":\"ack\",\"acked-senderId\":0,\"acked-timestamp\":1,\"timestamp\":2,\"senderId\":1}";
		AcknowledgeMessage expected = new AcknowledgeMessage(1, 2, 0, 1);
		Message message = mapper.readValue(json, Message.class);

		assertEquals(json, mapper.writeValueAsString(expected));

		assertEquals(1, message.senderId);
		assertEquals(2, message.timestamp);
		assertEquals(AcknowledgeMessage.class, message.getClass());

		AcknowledgeMessage ack = (AcknowledgeMessage) message;
		assertEquals(0, ack.ackedSenderId);
		assertEquals(1, ack.ackedTimestamp);
	}

	@Test
	public void testUserEvent() throws Exception {
		String json = "{\"type\":\"unicast\",\"event\":{" +
				"\"type\":\"user\",\"event\":{\"type\":\"bomb-dropped\",\"bomb\":{\"area\":\"BLUE\",\"id\":0},\"username\":\"test\"}}," +
				"\"timestamp\":1,\"senderId\":0}";
		Bomb blue = new Bomb(0, Area.BLUE);
		UserEvent e = new UserEvent(new BombDropped("test", blue));
		Message msg = new UnicastMessage(0, 1, e);

		assertEquals(json, mapper.writeValueAsString(msg));

		Message message = mapper.readValue(json, Message.class);

		assertEquals(message.senderId, 0);
		assertEquals(message.timestamp, 1);
		assertEquals(UnicastMessage.class, message.getClass());

		Event event = ((UnicastMessage) message).event;
		assertEquals(UserEvent.class, event.getClass());

		UserEvent userEvent = (UserEvent) event;
		assertEquals(BombDropped.class, userEvent.event.getClass());

		BombDropped bd = (BombDropped) userEvent.event;
		assertEquals("test", bd.username);
		assertEquals(blue, bd.bomb);
	}
}
