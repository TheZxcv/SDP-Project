package edu.sdp.project.game.events;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.sdp.project.game.Area;
import edu.sdp.project.game.Bomb;
import edu.sdp.project.utils.Point;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameEventsTest {

	private ObjectMapper mapper;

	@Before
	public void setUp() {
		mapper = new ObjectMapper();
		// deterministic properties order
		mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
	}

	@Test
	public void testMove() throws Exception {
		String json = "{\"type\":\"move\",\"pos\":{\"x\":0,\"y\":0},\"username\":\"test\"}";
		MoveEvent expected = new MoveEvent("test", new Point(0, 0));

		assertEquals(json, mapper.writeValueAsString(expected));

		GameEvent e = mapper.readValue(json, GameEvent.class);

		assertEquals(MoveEvent.class, e.getClass());
		MoveEvent me = (MoveEvent) e;
		assertEquals(expected.username, me.username);
		assertEquals(expected.pos, me.pos);
	}

	@Test
	public void testLaunchBomb() throws Exception {
		String json = "{\"type\":\"bomb-dropped\",\"bomb\":{\"area\":\"BLUE\",\"id\":0},\"username\":\"test\"}";
		BombDropped expected = new BombDropped("test", new Bomb(0, Area.BLUE));

		assertEquals(json, mapper.writeValueAsString(expected));

		GameEvent e = mapper.readValue(json, GameEvent.class);

		assertEquals(BombDropped.class, e.getClass());
		BombDropped bd = (BombDropped) e;
		assertEquals(expected.username, bd.username);
		assertEquals(expected.bomb, bd.bomb);
	}


	@Test
	public void testBombExplosion() throws Exception {
		String json = "{\"type\":\"bomb-explosion\",\"bomb\":{\"area\":\"BLUE\",\"id\":0},\"username\":\"test\"}";
		BombExplosion expected = new BombExplosion("test", new Bomb(0, Area.BLUE));

		assertEquals(json, mapper.writeValueAsString(expected));

		GameEvent e = mapper.readValue(json, GameEvent.class);

		assertEquals(BombExplosion.class, e.getClass());
		BombExplosion be = (BombExplosion) e;
		assertEquals(expected.username, be.username);
		assertEquals(expected.bomb, be.bomb);
	}
}
