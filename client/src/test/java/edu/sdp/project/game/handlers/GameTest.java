package edu.sdp.project.game.handlers;

import edu.sdp.project.game.Area;
import edu.sdp.project.game.Bomb;
import edu.sdp.project.game.PlayerState;
import edu.sdp.project.game.actions.GameAction;
import edu.sdp.project.game.actions.Movement;
import edu.sdp.project.game.events.BombDropped;
import edu.sdp.project.game.events.BombExplosion;
import edu.sdp.project.game.events.ExplosionHandlingRequest;
import edu.sdp.project.game.events.Hit;
import edu.sdp.project.game.events.LocatedAt;
import edu.sdp.project.game.events.Miss;
import edu.sdp.project.game.events.MoveEvent;
import edu.sdp.project.game.events.PlayerJoined;
import edu.sdp.project.game.events.Won;
import edu.sdp.project.model.Game;
import edu.sdp.project.model.Player;
import edu.sdp.project.ring.Peer;
import edu.sdp.project.utils.Point;
import edu.sdp.project.utils.Utils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GameTest {

	private GameState state;
	private Peer peer;
	private Player p1;
	private PlayerState ps1;

	@Before
	public void setUp() throws Exception {
		Game game = new Game("test", 2, 2);
		p1 = new Player("p1", "localhost", 12345);
		Player p2 = new Player("p2", "localhost", 54321);
		game.addPlayer(p1);
		game.addPlayer(p2);

		peer = mock(Peer.class);
		when(peer.isTokenSpinning()).thenReturn(true);
		ps1 = mock(PlayerState.class);
		when(ps1.getPlayer()).thenReturn(p1);

		state = new GameState(game, ps1, peer);
	}

	@Test
	public void testSpawning() throws Exception {
		Point taken = new Point(0, 0);

		state.handleTurn();
		state.handleLocatedAt(new LocatedAt(taken));

		verify(peer).send(any(PlayerJoined.class));
		verify(peer).passToken();

		ArgumentCaptor<Point> pos = ArgumentCaptor.forClass(Point.class);
		verify(ps1).setPosition(pos.capture());
		assertNotEquals(taken, pos.getValue());
	}

	@Test
	public void testMove() throws Exception {
		Point starting = new Point(0, 0);
		Point ending = new Point(0, 1);

		state.changeHandler(new Playing(state));
		submitMove(new Movement(Movement.Direction.DOWN));

		when(ps1.getPosition()).thenReturn(starting);

		state.handleTurn();
		state.handleMove(new MoveEvent(p1.getName(), ending));
		state.handleMiss(new Miss(p1.getName(), Hit.How.MELEE));

		InOrder io = inOrder(peer, ps1);
		ArgumentCaptor<Point> pos = ArgumentCaptor.forClass(Point.class);

		io.verify(peer).send(any(MoveEvent.class));
		io.verify(ps1).setPosition(pos.capture());
		io.verify(peer).passToken();
		io.verify(ps1, never()).addPoints(anyInt());

		assertEquals(ending, pos.getValue());
	}

	@Test
	public void testMoveAndBombSequence() throws Exception {
		Point starting = new Point(0, 1);
		Point ending = new Point(0, 0);

		Bomb bomb = new Bomb(0, Area.GREEN);
		when(ps1.getPosition()).thenReturn(starting, ending);

		state.changeHandler(new Playing(state));
		submitMove(new Movement(Movement.Direction.UP));

		state.handleTurn();
		state.handleBomb(new BombDropped("p2", bomb));
		state.handleMove(new MoveEvent(p1.getName(), ending));
		state.handleRequest(new ExplosionHandlingRequest("p2", bomb));
		state.handleHit(new Hit("p1", Hit.How.MELEE));
		state.handleTurn();
		state.handleExplosion(new BombExplosion("p2", bomb));

		InOrder io = inOrder(peer);
		io.verify(peer).send(any(MoveEvent.class));
		io.verify(peer).passToken();
		io.verify(peer).send(any(BombExplosion.class));
		io.verify(peer).send(any(Hit.class));
		io.verify(peer).leave();

		verify(ps1).addPoints(1);
		verify(ps1).die();
	}

	@Test
	public void testVictoryWhileDealingWithBombs() throws Exception {
		when(ps1.getPosition()).thenReturn(new Point(0, 0));

		state.changeHandler(new Playing(state));
		Bomb bomb1 = new Bomb(0, Area.BLUE);
		Bomb bomb2 = new Bomb(1, Area.BLUE);
		state.handleRequest(new ExplosionHandlingRequest("p2", bomb1));
		state.handleRequest(new ExplosionHandlingRequest("p2", bomb2));
		state.handleTurn();
		state.handleExplosion(new BombExplosion("p2", bomb1));
		state.handleWinning(new Won("p2"));

		InOrder io = inOrder(peer);
		io.verify(peer).send(new BombExplosion("p2", bomb1));
		io.verify(peer).send(new BombExplosion("p2", bomb2));
		io.verify(peer).leave();
	}

	private void submitMove(GameAction action) {
		Thread t = new Thread(() -> {
			try {
				state.addNextAction(action);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		t.start();

		while (!t.getState().equals(Thread.State.WAITING)) {
			Thread.yield();
			Utils.sleepWithoutHassle(50);
		}
	}
}
