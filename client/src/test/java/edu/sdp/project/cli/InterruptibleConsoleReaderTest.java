package edu.sdp.project.cli;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InterruptibleConsoleReaderTest {

	@Test
	public void testRead() throws Exception {
		InterruptibleConsoleReader console = new InterruptibleConsoleReader(new StringReader("1\n2\n3\n"));

		assertEquals("1", console.read());
		assertEquals("2", console.read());
		assertEquals("3", console.read());
		assertNull(console.read());
	}

	@Test
	public void testInterruption() throws Exception {
		final Object lock = new Object();
		BufferedReader reader = mock(BufferedReader.class);
		when(reader.readLine()).then((invocation -> {
			synchronized (lock) {
				lock.wait();
			}
			return null;
		}));

		InterruptibleConsoleReader console = new InterruptibleConsoleReader(reader);

		Thread th = new Thread(() -> assertNull(console.read()));
		th.start();
		do {
			Thread.yield();
			Thread.sleep(50);
		} while (th.getState() == Thread.State.RUNNABLE);

		console.interruptConsumer();
		th.join();

		synchronized (lock) {
			lock.notify();
		}
		console.close();
	}

	@Test
	public void testClose() throws Exception {
		InterruptibleConsoleReader console = new InterruptibleConsoleReader(new StringReader("1\n2\n3\n"));

		console.close();
		assertNull(console.read());
	}
}
