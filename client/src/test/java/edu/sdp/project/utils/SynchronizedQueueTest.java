package edu.sdp.project.utils;

import static java.util.stream.IntStream.range;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

public class SynchronizedQueueTest {

	private SynchronizedQueue<Integer> queue;

	@Before
	public void setUp() {
		queue = new SynchronizedQueue<>();
	}

	@Test(timeout = 200)
	public void testPollOnEmpty() throws Exception {
		assertNull(queue.poll());
	}

	@Test(timeout = 200)
	public void testPollOnOneElement() throws Exception {
		Integer ONE = 1;
		queue.put(ONE);
		assertEquals(queue.poll(), ONE);
		assertNull(queue.poll());
	}

	@Test
	public void testProducerConsumer() throws Exception {
		int length = 20;
		List<Integer> output = new ArrayList<>(length);
		Thread producer = new Thread(() -> range(0, length).forEach(this::putInQueue));
		Thread consumer = new Thread(() -> {
			for (int i = 0; i < length; i++)
				output.add(takeFromQueue());
		});

		consumer.start();
		producer.start();

		consumer.join();
		producer.join();

		List<Integer> expected = range(0, length).boxed().collect(Collectors.toList());
		assertEquals(expected, output);
	}

	@Test
	public void testDrainTo() throws Exception {
		queue.put(1);
		queue.put(2);
		queue.put(3);
		List<Integer> list = new ArrayList<>();
		queue.drainTo(list);

		assertEquals(Arrays.asList(1, 2, 3), list);
	}

	@Test
	public void testDrainToTillMax() throws Exception {
		queue.put(1);
		queue.put(2);
		queue.put(3);
		List<Integer> list = new ArrayList<>();
		queue.drainTo(list, 2);

		assertEquals(Arrays.asList(1, 2), list);
	}

	private void putInQueue(Integer n) {
		try {
			queue.put(n);
		} catch (InterruptedException e) {
			;
		}
	}

	private Integer takeFromQueue() {
		try {
			return queue.take();
		} catch (InterruptedException e) {
			;
		}
		return null;
	}
}
